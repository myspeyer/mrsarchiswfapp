rem *** Fix the timestamp space for hour between 1 and 9
set hour=%time:~0,2%
if "%hour%"==" 0" set hour=00
if "%hour%"==" 1" set hour=01
if "%hour%"==" 2" set hour=02
if "%hour%"==" 3" set hour=03
if "%hour%"==" 4" set hour=04
if "%hour%"==" 5" set hour=05
if "%hour%"==" 6" set hour=06
if "%hour%"==" 7" set hour=07
if "%hour%"==" 8" set hour=08
if "%hour%"==" 9" set hour=09

@SET filename=MRSArchisWFApp%date:~-4,4%-%date:~-7,2%-%date:~-10,2%T%hour%.%time:~3,2%.zip
@"C:\Program Files\7-Zip\7z" a -tzip "%filename%" "MRSArchisWFApp\*.*" > NUL:
@"C:\Program Files\7-Zip\7z" a -tzip "%filename%" -r "MRSArchisWFApp\Content\*.*" > NUL:
@"C:\Program Files\7-Zip\7z" a -tzip "%filename%" -r "MRSArchisWFApp\Controllers\*.*" > NUL:
@"C:\Program Files\7-Zip\7z" a -tzip "%filename%" -r "MRSArchisWFApp\Helpers\*.*" > NUL:
@"C:\Program Files\7-Zip\7z" a -tzip "%filename%" -r "MRSArchisWFApp\Models\*.*" > NUL:
@"C:\Program Files\7-Zip\7z" a -tzip "%filename%" -r "MRSArchisWFApp\Properties\*.*" > NUL:
@"C:\Program Files\7-Zip\7z" a -tzip "%filename%" -r "MRSArchisWFApp\Scripts\*.*" > NUL:
@"C:\Program Files\7-Zip\7z" a -tzip "%filename%" -r "MRSArchisWFApp\Service\*.*" > NUL:
@"C:\Program Files\7-Zip\7z" a -tzip "%filename%" -r "MRSArchisWFApp\Service References\*.*" > NUL:
@"C:\Program Files\7-Zip\7z" a -tzip "%filename%" -r "MRSArchisWFApp\Views\*.*" > NUL:
xcopy "%filename%" "C:\Users\m.y.speyer\SkyDrive\Projects\RCE\MRS ARCHIS\20 Product Generiek\04 Bouw\MRSArchisWFApp\Source"
@del "%filename%"

