<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output method="html" />
    <xsl:template match="/">
        <html>
            <head><title>Formulier.xml</title></head>
            <link href="C:\AppDev\Visual Studio\Projects\MUSWF\MvcMUSWFapp\MvcMUSWFapp\Content/Site.css" rel="stylesheet" type="text/css" />
                
            <body style="background-color: white;">
                <div class="page">
                    <header />
                    <section id="main">
                        <xsl:apply-templates/>
                    </section>
                    <footer>Laatste wijziging: <xsl:value-of select="/zaak/zaakFormulier/laatsteWijziging"/></footer>
                </div>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="/zaak/zaakType/naam">
        <h1>
            <xsl:value-of select="."/><xsl:text> </xsl:text>
            <xsl:value-of select="/zaak/zaakKenmerken/subregelingAanvraagjaarOmschrijving"/>            
        </h1>
    </xsl:template>
    
    <xsl:template match="zaak/zaakRolSubject">
        <xsl:comment>Ignore subjectId</xsl:comment>
    </xsl:template>
    
    <xsl:template match="zaakInfo">
        <p>Zaaknummer: <xsl:value-of select="substring(zaakNummer, 31)"/></p>
    </xsl:template>
    
    <xsl:template match="zaakKenmerken">
        <xsl:comment>Ignore zaakKenmerken</xsl:comment>
    </xsl:template>
    
    <xsl:template match="zaakFormulier">
        <table>
            <xsl:apply-templates select="zaakRolSubject"></xsl:apply-templates>
            <xsl:if test="//planopstellerAanwezig = 'true'">
                <tr>
                    <td>planopsteller</td>
                    <td><xsl:value-of select="//planopsteller"/></td>
                </tr>
                
            </xsl:if>
            <tr>
                <td>verklaring bijgevoegd</td>
                <td>
                    <xsl:choose>
                        <xsl:when test="//verklaring = 'true'">Ja</xsl:when>
                        <xsl:otherwise>Nee</xsl:otherwise>
                    </xsl:choose>
                </td>
            </tr>
            <tr>
                <td>aaanvrager is eigenaar</td>
                <td>
                    <xsl:choose>
                        <xsl:when test="//typeAanvraagRelatie = 'EIGENAAR'">Ja</xsl:when>
                        <xsl:otherwise>Nee</xsl:otherwise>
                    </xsl:choose>
                </td>
            </tr>
            <xsl:apply-templates select="zaakObjecten"></xsl:apply-templates>
        </table>
    </xsl:template>
    
    <xsl:template match="formulierId|laatsteWijziging">
        <xsl:comment>Ignore fomulierId</xsl:comment>
    </xsl:template>
    
    <xsl:template match="typeSubject|rol">
        
    </xsl:template>
    
    <xsl:template match="zaakFormulier/zaakRolSubject">
        <xsl:variable name="typeSubject">
            <xsl:value-of select="typeSubject"/>
        </xsl:variable>
        <tr>
            <th colspan="2">
                Gegevens van <xsl:value-of select="rol"/>
                <xsl:choose>
                    <xsl:when test="$typeSubject = 'natuurlijkpersoon'">
                        <xsl:text> (persoon)</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text> (bedrijf)</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </th>            
        </tr>
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="contactId|huisnummer|huisnummerToevoeging|woonplaats|gemeenteCode|provincieCode">
        
    </xsl:template>
    
    <xsl:template match="bsn|voornaam|achternaam|voorletters|geslacht|emailadres|telefoonnummer|bankrekeningnr|straatnaam|postcode|gemeente|provincie">
        <tr>
            <td><xsl:value-of select="name(.)"/></td>
            <td>
                <xsl:choose>
                    <xsl:when test="name(.) = 'straatnaam'">
                        <xsl:value-of select="."/>:
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="following-sibling::huisnummer"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="following-sibling::huisnummerToevoeging"/>
                    </xsl:when>
                    <xsl:when test="name(.) = 'postcode'">
                        <xsl:value-of select="."/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="following-sibling::woonplaats"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="."/>                        
                    </xsl:otherwise>
                </xsl:choose>
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template match="monumentSoort">
        <tr>
            <th colspan="2">Monument: <xsl:value-of select="."/></th>            
        </tr>
    </xsl:template>

    <xsl:template match="monumentNummer">
        <tr>
            <td>nummer:</td>
            <td><xsl:value-of select="."/></td>            
        </tr>
    </xsl:template>
    
    <xsl:template match="monument/naam">
        <tr>
            <td>naam:</td>
            <td><xsl:value-of select="."/></td>            
        </tr>
    </xsl:template>
    
    <xsl:template match="omschrijvingZelfstandigOnderdeel">
        <tr>
            <td>omschrijving zelfstandig onderdeel</td>
            <td><xsl:value-of select="."/></td>            
        </tr>        
    </xsl:template>
    
    <xsl:template match="monumentGebouwd">
        <tr>
            <td>Gegegevens over gebouwd monument</td>
            <td>
                <xsl:if test="isBewoond = 'true'">
                    Bewoond monument:<br/>
                    <xsl:value-of select="omschrijvingBewoond"/>
                </xsl:if>
            </td>            
        </tr>
    </xsl:template>
    
    <xsl:template match="monument/document">
        
    </xsl:template>
    
    <xsl:template match="monument/antwoord">
        <tr>
            <td colspan="2" style="border:none;">
                <table style="width: 100%; border:none">
                    <tr>
                        <td style="border:none;">
                            <xsl:value-of select="vraag/tekst" disable-output-escaping="yes"/>
                        </td>
                        <td style="width:12em; text-align:right; border:none;">
                            <xsl:apply-templates select="blok/antwoordTekst"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template match="antwoordTekst">
        <xsl:choose>            
            <xsl:when test=". = 'true'">Ja</xsl:when>
            <xsl:when test=". = 'false'">Nee</xsl:when>
            <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="zaakDocumenten">
        <h2>Lijst van bijlagen</h2>
        <ul>
            <xsl:apply-templates></xsl:apply-templates>
        </ul>
    </xsl:template>
    
    <xsl:template match="document">
        <li>
            <xsl:value-of select="bestandsnaam"/>, 
            aanmaakdatum: 
            <xsl:value-of select="aanmaakDatum"/>
            (documentId: <xsl:value-of select="documenttypeId"/>)
        </li>
    </xsl:template>
</xsl:stylesheet>