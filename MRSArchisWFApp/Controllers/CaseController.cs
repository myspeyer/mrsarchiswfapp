﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MRSArchisWFApp.Models.EPS;
using MRSArchisWFApp.Models.CaseService;
using MRSArchisWFApp.Helpers;
using MRSArchisWFApp.Helpers.EPS;
using MRSArchisWFApp.Helpers.MRSArchis;

namespace MRSArchisWFApp.Controllers
{
  public class CaseController : Controller
  {
    #region log4net
    // Create a logger for this class
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    #endregion

    /// <summary>
    /// General case summarry overview. This view is for diaply only and
    /// does not have form element.
    /// </summary>
    /// <returns>The case summary view ("Zaak")</returns>
    public ActionResult Overview()
    {
      try
      {
        CaseData cdata = CaseDataHelper.CaseData;
        return View(cdata);
      }
      catch (Exception ex)
      {
        ViewBag.message = ex.Message;
      }
      return View("Error");
    }

    /// <summary>
    /// Prepare the view for creating a new case with registration and
    /// receipt data initialized.
    /// </summary>
    /// <returns>View for creating a new case ("Nieuwe zaak")</returns>
    public ActionResult CreateNewCase()
    {
      try
      {
        CaseData cdata = CaseDataHelper.CaseData;

        // Initalize null registration and receipt date to today
        if (!cdata.registrationDate.HasValue)
          cdata.registrationDate = DateTime.Now;
        if (!cdata.receiptDate.HasValue)
          cdata.receiptDate = DateTime.Now;

        // Initialize null scan request and receipt note to true
        if (!cdata.receiptNote.HasValue)
          cdata.receiptNote = false;
        if (!cdata.scanRequest.HasValue)
          cdata.scanRequest = false;

        if (!string.IsNullOrEmpty(cdata.caseNumber))
        {
          cdata.caseReference = CaseHelper.MainCaseNumber;
        }
        return View(cdata);
      }
      catch (Exception ex)
      {
        ViewBag.message = ex.Message;
      }

      return View("Error");
    }
    /// <summary>
    /// Create a case number and folder
    /// </summary>
    /// <returns>JSON with case number ("zaaknummer") an case year id (subregelingAanvraagJaarId)</returns>
    public ActionResult GetCaseNumber(
      string caseReference, 
      string caseTypeName, 
      string roleTypeName, 
      string statusTypeName, 
      string registrationDate, 
      string receiptDate)
    {
      try
      {
        CaseData cdata = CaseDataHelper.CaseData;

        cdata.roleTypeName = roleTypeName;
        cdata.roleTypeId = CaseHelper.GetRoleTypeId(roleTypeName);
        cdata.caseTypeName = caseTypeName;
        cdata.caseTypeId = CaseHelper.GetCaseTypeId(caseTypeName, cdata.roleTypeId);
        cdata.statusTypeName = statusTypeName;
        cdata.statusTypeId = CaseHelper.GetStatusTypeId(cdata.statusTypeName);
        cdata.caseReference = caseReference;

        cdata.registrationDate = MRSArchis.OpenText.Helpers.UtilityHelper.StringToNullableDate(registrationDate);
        cdata.receiptDate = MRSArchis.OpenText.Helpers.UtilityHelper.StringToNullableDate(receiptDate);
        if (!cdata.registrationDate.HasValue || !cdata.receiptDate.HasValue)
          throw new Exception("Ongeldige registratie- of ontvangstdatum");

        //cdata.caseNumber = new Random().Next(10000, 99999).ToString() + new Random().Next(10000, 99999).ToString();
        if (string.IsNullOrEmpty(cdata.caseReference))
        {
          cdata.caseNumber = CaseHelper.CreateCase(
            2,                                    /* 0-9, determines the first digit of the generated case number */
            cdata.registrationDate,               /* Date case is registered */
            cdata.receiptDate,                    /* Date case was received */
            cdata.roleTypeId,                     /* The id of the role name */
            cdata.caseTypeId.GetValueOrDefault(), /* The id of the case type */
            cdata.statusTypeId,                   /* The id of the case status */
            caseReference                         /* The reference case number for a subsequent case number */
            );
          SessionHandler.CSHelperObject.CreateCaseFolder(cdata.caseNumber);
        }
        /*
        else
        {
          if (string.IsNullOrEmpty(cdata.caseReference))
            throw new Exception("Gerelateerd zaaknummer is verplicht voor wijziging, melding of vaststelling");

          try
          {
            CaseDetail caseDetail = Helpers.SACE.CaseHelper.GetCaseDetail(cdata.caseReference);
            CaseHelper.SetCaseYearName(caseDetail.caseYearNameId.GetValueOrDefault().ToString(), ref cdata);
          }
          catch
          {
            throw new Exception("Het gerelateerde zaaknummer bestaat niet");
          }
          cdata.caseNumber = Helpers.SACE.CaseHelper.GetSequenceNumber(cdata.caseReference, (CaseTypes)Enum.Parse(typeof(CaseTypes), cdata.caseType));
          CSHelper.CreateCaseFolder(cdata.caseNumber);
        }
       */
        SessionHandler.CaseNumber = cdata.caseNumber;

        // Save it to process and CS to fix values
        CaseDataHelper.SaveToDB();

        return new JsonResult
        {
          JsonRequestBehavior = JsonRequestBehavior.AllowGet,
          Data = new { error = false, caseNumber = cdata.caseNumber }
        };
      }
      catch (Exception ex)
      {
        return new JsonResult
        {
          JsonRequestBehavior = JsonRequestBehavior.AllowGet,
          Data = new { error = true, message = ex.Message }
        };
      }
    }

    public ActionResult GetCaseRoleList(long? caseTypeId)
    {
      return Json(CaseHelper.GetCaseRoleTypes(caseTypeId), JsonRequestBehavior.AllowGet);
    }

    public ActionResult IntakeMRSCase()
    {
      try
      {
        CaseData cdata = CaseDataHelper.CaseData;
        return View(cdata);
      }
      catch (Exception ex)
      {
        ViewBag.message = ex.Message;
      }
      return View("Error");
    }

    public ActionResult IntakeAddMonument()
    {
      try
      {
        CaseData cdata = CaseDataHelper.CaseData;
        return View(cdata);
      }
      catch (Exception ex)
      {
        ViewBag.message = ex.Message;
      }
      return View("Error");
    }

    public ActionResult IntakeChangeMonument()
    {
      try
      {
        CaseData cdata = CaseDataHelper.CaseData;
        return View(cdata);
      }
      catch (Exception ex)
      {
        ViewBag.message = ex.Message;
      }
      return View("Error");
    }

    public ActionResult IntakeDeleteMonument()
    {
      try
      {
        CaseData cdata = CaseDataHelper.CaseData;
        return View(cdata);
      }
      catch (Exception ex)
      {
        ViewBag.message = ex.Message;
      }
      return View("Error");
    }
  }
}
