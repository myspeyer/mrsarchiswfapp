﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;
using MRSArchisWFApp.Helpers;
using MRSArchisWFApp.Helpers.EPS;
using MRSArchisWFApp.Models.Checklist;
using MRSArchisWFApp.Models.EPS;

namespace MRSArchisWFApp.Controllers
{
  public class CheckListController : Controller
  {
    public ActionResult Edit(string name)
    {
      try
      {
        CheckList checklist;
        if (!string.IsNullOrEmpty(name))
          checklist = CheckListHelper.CheckLists.FirstOrDefault(
            c => c.Name == name);
        else
          checklist = CheckListHelper.CheckLists.FirstOrDefault(
            c => c.WorkflowStepName == SessionHandler.CurrentStep);
        if (checklist != null)
        {
          if (checklist.WorkflowStepName == SessionHandler.CurrentStep)
          {
            // Get the case data to set the checklist
            CaseData cdata = CaseDataHelper.CaseData;
            cdata.activeCheckList = checklist.Name;
          }
          return View(checklist);
        }
        else 
          return View(CheckListHelper.CheckLists.First());
      }
      catch (Exception ex)
      {
        ViewBag.message = ex.Message + " (CheckListController.Edit, HtppGet)";
      }
      return View("Error");
    }

    [HttpPost]
    public ActionResult Edit(string name, FormCollection collection)
    {
      try
      {
        CheckList checklist = CheckListHelper.GetCacheObject(collection["cacheid"]);
        // Handle answers seperately
        checklist.isDirty = CheckListHelper.UpdateCheckListAnswers(checklist.Questions, collection);
        //CheckListHelper.Update(checklist);
        return View(checklist);
      }
      catch (Exception ex)
      {
        ViewBag.message = ex.Message + " (CheckListController.Edit, HtppPost)";
      }
      return View("Error");
    }

  }
}
