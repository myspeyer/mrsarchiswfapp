﻿using MRSArchisWFApp.CHOService;
using MRSArchisWFApp.Helpers;
using MRSArchisWFApp.Helpers.CHO;
using MRSArchisWFApp.Helpers.EPS;
using MRSArchisWFApp.Helpers.MRSArchis;
using MRSArchisWFApp.MRSArchisWS;
using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MRSArchisWFApp.Controllers
{
    public class DicoController : Controller
    {
        public ActionResult ShowWGPViewer()
        {
            return View();
        }

        public ActionResult Index()
        {
            return View(MonumentHelper.Monuments);
        }

        public ActionResult Edit(string uploadresult, string cacheId)
        {
            try
            {
                ViewBag.message = uploadresult;

                return View(MonumentHelper.GetCacheObject(cacheId));
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
            }
            return View("Error");
        }
        
        [HttpPost]
        // When using rijksmonument as a parameter geometrie and locationDescription doesn't get filled, so used FormCollection
        public ActionResult Edit(string uploadresult, string cacheId, FormCollection fc)        
        {
            try
            {
                string beschrijving = fc.GetValue("beschrijving").AttemptedValue;
                int herkomstId = Convert.ToInt32(fc.GetValue("geometrie.herkomstId").AttemptedValue);
                int mateOvernameId = Convert.ToInt32(fc.GetValue("geometrie.mateOvernameId").AttemptedValue);
                int kwaliteitGeometrieId = Convert.ToInt32(fc.GetValue("geometrie.kwaliteitGeometrieId").AttemptedValue);

                var monument = MonumentHelper.GetCacheObject(cacheId);
                
                monument.geometrie.herkomstId = herkomstId;
                monument.geometrie.herkomstIdSpecified = true;
                monument.geometrie.mateOvernameId = mateOvernameId;
                monument.geometrie.mateOvernameIdSpecified = true;
                monument.geometrie.kwaliteitGeometrieId = kwaliteitGeometrieId;
                monument.geometrie.kwaliteitGeometrieIdSpecified = true;
                monument.geometrie.isDirty = true;

                var document = DocumentHelper.Documents.First(d => d.id == monument.geometrie.documentId);
                document.beschrijving = beschrijving;
                document.isDirty = true;

                return RedirectToAction("Edit", new { uploadresult = "", cacheId = cacheId });
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
            }
            return View("Error");
        }

        public ActionResult Delete(string cacheId)
        {
            try
            {
                return View(MonumentHelper.GetCacheObject(cacheId));
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
            }
            return View("Error");            
        }

        [HttpPost]
        public ActionResult Delete(string cacheId, FormCollection formCollection)
        {
            try
            {
                MonumentHelper.Delete(cacheId);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
            }
            return RedirectToAction("Index");
        }

        public ActionResult Upload(string cacheId)
        {
            try
            {
                return View(MonumentHelper.GetCacheObject(cacheId));
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
            }
            return View("Error");            
        }
        
        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file_upload, string cacheId)
        {
            try
            {
                var fileName = Path.GetFileName(file_upload.FileName);
                var fileExtension = Path.GetExtension(fileName).ToLower();
                if (ExtensionAllowed(fileExtension))
                {
                    var docId = UploadToContentServer(file_upload, fileName);
                    UploadToDatabase(file_upload, ref cacheId, docId);                    

                    return RedirectToAction("Edit", new { uploadresult = Url.Encode(fileName) + " is geüpload.", cacheId = cacheId });
                }
                return RedirectToAction("Edit", new { uploadresult = Url.Encode(fileExtension.Substring(1).ToUpper()) + " bestanden kunnen niet worden geüpload.", cacheId = cacheId });
            }
            catch (Exception ex)
            {
                // TODO: Delete
                ViewBag.message = ex.Message;
            }
            return View("Error");
        }

        public HtmlString GetWidget()
        {            
            return new HtmlString("<script src='http://rce.webgispublisher.nl/widget.ashx?map=MRS-Archis_Aanwijzen_Widget10&bbox=230263.7,554362.2,237682.5,561135.9&ovautoopen=false&slider=smallSlider&doPanel=panelNone&width=200&height=300&ovheight=150&ovwidth=150&glgeol=' language='javascript' id='wgpscript'></script><noscript><p>Uw browser ondersteunt geen JavaScript!</p><p>De (niet getoonde) kaart bevat het volgende:</p><p>MRS/Archis kaart t.b.v. proces Aanwijzen monumenten - Behandelen</p></noscript>");
        }

        private bool ExtensionAllowed(string extension)
        {
            string AllowedFileExtensions = UtilityHelper.AllowedUploadExtensions;

            return AllowedFileExtensions.Split(',').FirstOrDefault(s => s.IndexOf(extension) != -1) != null;
        }

        private void UploadToDatabase(HttpPostedFileBase file, ref string cacheId, long docId)
        {
            CHOServiceClient choService = new CHOServiceClient();
            ZaakServiceClient zaakService = new ZaakServiceClient();
                        
            geometrie geometry;
            rijksmonument monument;

            if (!string.IsNullOrEmpty(cacheId))
            {
                monument = MonumentHelper.GetCacheObject(cacheId);
                geometry = monument.geometrie;
            }
            else
            {
                long zaakId = CaseHelper.GetCaseId(CaseDataHelper.CaseData.caseNumber).GetValueOrDefault();
                
                monument = InsertNewRijksmonument(choService, zaakId);
                monument = MonumentHelper.AddToCache(monument);
                cacheId = monument.cacheId;
                
                var choID = monument.choId; // TODO: I think value choID shouldn't be "ref".
                zaakService.insertZaakobject(zaakId, ref choID, monument.choId);

                geometry = new geometrie();
                geometry.choId = monument.choId;
            }            
            
            // Todo: remove convert
            geometry.statusGeometrieId = Convert.ToInt32(StatesGeometryHelper.getStateGeometryId(StatesGeometryHelper.voorbeschermingsDico01));
            geometry.statusGeometrieIdSpecified = true;
            geometry.beginGeldigheid = DateTime.Now;
            geometry.beginGeldigheidSpecified = true;
            geometry.documentId = docId;
            
            geometry.eindGeldigheidSpecified = false;
            geometry.geometrieIdSpecified = false;            
            geometry.herkomstIdSpecified = false;
            geometry.mateOvernameIdSpecified = false;
            geometry.kwaliteitGeometrieIdSpecified = false;
            
            geometry.gmObject = ReadFileContent(file);

            
            geometry.geometrieId = choService.insertOrUpdateGeometrie(SessionHandler.Username, geometry);            
            geometry.geometrieIdSpecified = true;
                        
            // TODO: Als insertGeometrie klapt dan ook monument verwijderen! Maar ook alleen als monument voorheen nog niet bestond!

            monument.geometrie = geometry;
        }

        private rijksmonument InsertNewRijksmonument(CHOServiceClient choService, long zaakId)
        {
            var monument = new rijksmonument();

            monument.aardMonumentIdSpecified = false;
            monument.bebouwdeKomIdSpecified = false;            
            monument.choIdSpecified = false;
            monument.identificatieImkich = "";
            monument.juridischeStatusIdSpecified = false;
            monument.objectbegingeldigheidSpecified = false;
            monument.objecteindgeldigheidSpecified = false;
            monument.rijksmonumentNummerSpecified = false;
            monument.specialisatieType = Convert.ToInt32(SpecializationTypeHelper.getId(SpecializationTypeHelper.monument)); // 2; // TODO: Remove Convert
            monument.vertrouwelijkheidsAanduidingIdSpecified = false;
            monument.zaakId = zaakId;
                        
            monument.choId = choService.insertOrUpdateRijksmonument(SessionHandler.Username, monument);
            monument.choIdSpecified = true;
            
            return monument;
        }

        private string ReadFileContent(HttpPostedFileBase file)
        {
            // Reposition InputStream to start of file.
            file.InputStream.Position = 0;
            
            return new StreamReader(file.InputStream).ReadToEnd();
        }
        
        private long UploadToContentServer(HttpPostedFileBase file_upload, string fileName)
        {
            string nodeId = SessionHandler.CSHelperObject.Upload(
              SessionHandler.CSHelperObject.GetCaseFolder(SessionHandler.CaseNumber), file_upload.InputStream, fileName);
            var doc = DocumentHelper.AddFile(file_upload, int.Parse(nodeId));
                                
            var cdata = Helpers.EPS.CaseDataHelper.CaseData;
            cdata.numAttachments = DocumentHelper.Documents.Count;

            return doc.id;
        }
    }
}
