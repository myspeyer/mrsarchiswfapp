﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MRSArchisWFApp.Helpers;
using MRSArchisWFApp.Helpers.MRSArchis;
using MRSArchisWFApp.MRSArchisWS;

namespace MRSArchisWFApp.Controllers
{
    public class DocumentController : Controller
    {
      public ActionResult Index()
      {
        try
        {
          // Update the case data in case it is out of sync
          var cdata = Helpers.EPS.CaseDataHelper.CaseData;
          cdata.numAttachments = DocumentHelper.Documents.Count;
          return View(DocumentHelper.Documents.OrderBy(d => d.titel.ToUpper()));
        }
        catch (Exception ex)
        {
          ViewBag.message = ex.Message;
        }
        return View("Error");
      }

      public ActionResult Edit(string id)
      {
        try
        {
          return View(DocumentHelper.GetCacheObject(id));
        }
        catch (Exception ex)
        {
          ViewBag.message = ex.Message;
        }
        return View("Error");
      }

      [HttpPost]
      public ActionResult Edit(string id, int documenttypeId, FormCollection collection)
      {
        try
        {
          document doc = DocumentHelper.GetCacheObject(id);
          Helpers.UtilityHelper.FormatCollectionForModel(collection);
          TryUpdateModel(doc, collection);
          TryUpdateModel(doc);
          // Set the subtype field 
          doc.subType = DocumentTypesHelper.GetDocumentSubTypeFor(documenttypeId);
          return RedirectToAction("Index");
        }
        catch (Exception ex)
        {
          ViewBag.message = ex.Message;
        }
        return View("Error");
      }

      public ActionResult Delete(string id)
      {
        try
        {
          return View(DocumentHelper.GetCacheObject(id));
        }
        catch (Exception ex)
        {
          ViewBag.message = ex.Message;
        }
        return View("Error");
      }

      [HttpPost]
      public ActionResult Delete(string id, FormCollection collection)
      {
        try
        {
          DocumentHelper.Delete(id);

          // Update databse and case data
          var cdata = Helpers.EPS.CaseDataHelper.CaseData;
          cdata.numAttachments = DocumentHelper.Documents.Count;
          return RedirectToAction("Index");
        }
        catch (Exception ex)
        {
          ViewBag.message = ex.Message;
        }
        return View("Error");
      }

      public ActionResult GetDocumentSubTypes(string mainType)
      {
        return Json(DocumentTypesHelper.GetDocumentSubTypesFor(mainType), JsonRequestBehavior.AllowGet);
      }
    }
}
