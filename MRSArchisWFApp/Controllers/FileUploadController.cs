﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MRSArchisWFApp.Helpers;

namespace MRSArchisWFApp.Controllers
{
    public class FileUploadController : Controller
    {
      public ActionResult Index(string multipleUploader)
      {
        try
        {
          using (CuteWebUI.MvcUploader uploader = new CuteWebUI.MvcUploader(System.Web.HttpContext.Current))
          {
            uploader.UploadUrl = Response.ApplyAppPathModifier("~/UploadHandler.ashx");
            //the data of the uploader will render as <input type='hidden' name='myuploader'> 
            uploader.Name = "multipleUploader";
            uploader.AllowedFileExtensions = UtilityHelper.AllowedUploadExtensions;
            uploader.MaxSizeKB = UtilityHelper.UploadMaxSizeKB;
            uploader.InsertText = "Selecteer bestanden om te uploaden";
            uploader.FileTypeNotSupportMsg = "Dit bestandtype is niet toegestaan. Toegestane extensies zijn: " + uploader.AllowedFileExtensions;
            //allow select multiple files
            uploader.MultipleFilesUpload = true;
            //tell uploader attach a button
            uploader.InsertButtonID = "uploadbutton";
            //prepair html code for the view
            ViewData["uploaderhtml"] = uploader.Render();
            //if it's HTTP POST:
            if (!string.IsNullOrEmpty(multipleUploader))
            {
              List<string> processedfiles = new List<string>();
              List<string> skippedExtensions = new List<string>();
              //for multiple files , the value is string : guid/guid/guid 
              foreach (string strguid in multipleUploader.Split('/'))
              {
                Guid fileguid = new Guid(strguid);
                CuteWebUI.MvcUploadFile file = uploader.GetUploadedFile(fileguid);
                if (file != null)
                {
                  string extension = Path.GetExtension(file.FileName).ToLower();
                  if (uploader.AllowedFileExtensions.Split(',').FirstOrDefault(s => s == extension.Substring(1)) != null)
                  {
                    // For testing
                    //string path = Server.MapPath("~/UploadedFiles/");
                    //file_upload.SaveAs(path + file.FileName);
                    string nodeId = SessionHandler.CSHelperObject.Upload(
                      SessionHandler.CSHelperObject.GetCaseFolder(SessionHandler.CaseNumber), file.OpenStream(), file.FileName);
                    DocumentHelper.AddFile(file, int.Parse(nodeId));
                    processedfiles.Add(file.FileName);
                  }
                  else if (!skippedExtensions.Exists(e => string.Compare(e, extension, true) == 0))
                    skippedExtensions.Add(Path.GetExtension(file.FileName));
                }
              }
              if (processedfiles.Count > 0)
              {
                // TODO: Save immediately to db avoid inconsistency if user doesn't save data
                
                var cdata = Helpers.EPS.CaseDataHelper.CaseData;
                cdata.numAttachments = DocumentHelper.Documents.Count;
                ViewBag.message = string.Join(",", processedfiles.ToArray()) + (processedfiles.Count == 1 ? " is" : " zijn") + " geüpload. ";
              }
              ViewBag.message += skippedExtensions.Count > 1 ? string.Join(",", skippedExtensions.ToArray()) + " bestanden kunnen niet worden geüpload." : string.Empty;
            }
          }
          return View();
        }
        catch (Exception ex)
        {
          ViewBag.message = ex.Message;
        }

        return View("Error");
      }

      public ActionResult ClassicUpload(string uploadresult)
      {
        ViewBag.message = uploadresult;
        return View();
      }

      [HttpPost]
      public ActionResult ClassicUpload(HttpPostedFileBase file_upload)
      {
        try
        {
          string AllowedFileExtensions = UtilityHelper.AllowedUploadExtensions;
          var fileName = Path.GetFileName(file_upload.FileName);
          string extension = Path.GetExtension(fileName).ToLower();
          if (AllowedFileExtensions.Split(',').FirstOrDefault(s => s.IndexOf(extension) != -1) != null)
          {
            // For testing
            //string path = Server.MapPath("~/UploadedFiles/");
            //file_upload.SaveAs(path + fileName);
            string nodeId = SessionHandler.CSHelperObject.Upload(
              SessionHandler.CSHelperObject.GetCaseFolder(SessionHandler.CaseNumber), file_upload.InputStream, fileName);
            DocumentHelper.AddFile(file_upload, int.Parse(nodeId));
            // TO DO: Save immediately to db avoid inconsistency if user doesn't save data

            var cdata = Helpers.EPS.CaseDataHelper.CaseData;
            cdata.numAttachments = DocumentHelper.Documents.Count;
            return RedirectToAction("ClassicUpload", new { uploadresult = Url.Encode(fileName) + " is geüpload." });
          }
          return RedirectToAction("ClassicUpload", new { uploadresult = Url.Encode(extension.Substring(1).ToUpper()) + " bestanden kunnen niet worden geüpload." });
        }
        catch (Exception ex)
        {
          ViewBag.message = ex.Message;
        }

        return View("Error");
      }
    }
}
