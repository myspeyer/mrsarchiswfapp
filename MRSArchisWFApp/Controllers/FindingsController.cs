﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MRSArchisWFApp.Helpers;
using MRSArchisWFApp.Helpers.EPS;
using MRSArchisWFApp.Models;
using MRSArchisWFApp.Models.EPS;

namespace MRSArchis.Controllers
{
    public class FindingsController : Controller
    {
      public ActionResult Index(string currentStep)
      {
        try
        {
          // Check if we can create new items
          CaseData cdata = CaseDataHelper.CaseData;
          return View(FindingsHelper.Findings);
        }
        catch (Exception ex)
        {
          ViewBag.message = ex.Message;
        }
        return View("Error");
      }

      public ActionResult Create()
      {
        try
        {
          Finding fi = new Finding { isNew = true };
          fi.Date = DateTime.Now;
          fi.WorkflowStepName = SessionHandler.CurrentStep;
          FindingsHelper.AddToCache(fi);

          // Get the case data to set the checklist for this step if any
          CaseData cdata = CaseDataHelper.CaseData;
          fi.Checklist = cdata.activeCheckList;

          return View(fi);
        }
        catch (Exception ex)
        {
          ViewBag.message = ex.Message;
        }
        return View("Error");
      }

      [HttpPost]
      public ActionResult Create(string cacheId, FormCollection collection)
      {
        try
        {
          Finding fi = FindingsHelper.GetCacheObject(cacheId);
          TryUpdateModel(fi);
          fi.Creator = SessionHandler.UserDisplayName;
          fi.Date = DateTime.Now;

          // Get the case data to set update it if needed
          CaseData cdata = CaseDataHelper.CaseData;

          return RedirectToAction("Index");
        }
        catch (Exception ex)
        {
          ViewBag.message = ex.Message;
        }
        return View("Error");
      }

      public ActionResult Edit(string id)
      {
        try
        {
          Finding fi = FindingsHelper.GetCacheObject(id);
          CaseData cdata = CaseDataHelper.CaseData;
          return View(fi);
        }
        catch (Exception ex)
        {
          ViewBag.message = ex.Message;
        }
        return View("Error");
      }

      [HttpPost]
      public ActionResult Edit(string id, FormCollection collection)
      {
        try
        {
          Finding fi = FindingsHelper.GetCacheObject(id);
          TryUpdateModel(fi);

          // Get the case data to set update it if needed
          CaseData cdata = CaseDataHelper.CaseData;

          fi = FindingsHelper.Findings.LastOrDefault();

          return RedirectToAction("Index");
        }
        catch (Exception ex)
        {
          ViewBag.message = ex.Message;
        }
        return View("Error");
      }

      public ActionResult Details(string id)
      {
        try
        {
          return View(FindingsHelper.GetCacheObject(id));
        }
        catch (Exception ex)
        {
          ViewBag.message = ex.Message;
        }
        return View("Error");
      }

      public ActionResult Delete(string id)
      {
        try
        {
          return View(FindingsHelper.GetCacheObject(id));
        }
        catch (Exception ex)
        {
          ViewBag.message = ex.Message;
        }
        return View("Error");
      }

      [HttpPost]
      public ActionResult Delete(string id, FormCollection collection)
      {
        try
        {
          FindingsHelper.Delete(id);

          // Get the case data to set update it if needed
          CaseData cdata = CaseDataHelper.CaseData;
          Finding fi = FindingsHelper.Findings.LastOrDefault();
        }
        catch (Exception ex)
        {
          ViewBag.message = ex.Message;
        }
        return RedirectToAction("Index");
      }
    }
}
