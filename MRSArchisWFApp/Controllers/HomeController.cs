﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MRSArchis.OpenText.EPS;
using MRSArchisWFApp.Models.CS;
using MRSArchisWFApp.Models.EPS;
using MRSArchisWFApp.Helpers;
using MRSArchisWFApp.Helpers.EPS;
using MRSArchisWFApp.Helpers.MRSArchis;

namespace MRSArchisWFApp.Controllers
{
  public class HomeController : Controller
  {
    // Create a logger for this class
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public ActionResult Index(string workitemid, string epsacctoken, string isreadonly)
    {
      try
      {
        // Redirect to the correct page
        if (workitemid != null)
        {
          if (log.IsInfoEnabled) log.InfoFormat("SetupSessionContext for CurrentStep: {0}", SessionHandler.CurrentStep);
          HomeHelper.SetupSessionContext(workitemid, epsacctoken, isreadonly);

          switch (SessionHandler.CurrentStep)
          {
            case "initiator_ZR_nz":
              return RedirectToAction("CreateNewCase", "Case");

            case "user_MI_in":
              return RedirectToAction("IntakeMRSCase", "Case");

            case "user_AM_in":
              return RedirectToAction("IntakeAddMonument", "Case");

            case "user_WM_in":
              return RedirectToAction("IntakeChangeMonument", "Case");

            case "user_VM_in":
              return RedirectToAction("IntakeDeleteMonument", "Case");
          }
        }
        return View();
      }
      catch (Exception ex)
      {
        // Write to log file
        if (log.IsDebugEnabled) log.DebugFormat(
          string.Format("(workitemdid:{0}, epsacctoken:{1}, isreadonly:{2}) - {3}",
            workitemid, epsacctoken, isreadonly, ex.Message));

        ViewBag.message = ex.Message;
      }
      return View("Error");
    }

    public ActionResult PWAction(string func)
    {
      if (log.IsDebugEnabled) log.Debug("PWAction: " + func);
      try
      {
        if (func == "Sendup" || func == "SendupTest")
        {
          CaseDataHelper.Validate();
          CheckListHelper.Validate();
          HomeHelper.SaveCache();
          if (func != "SendupTest")
          {
            EPSHelper.SendOn(SessionHandler.WorkItemId, SessionHandler.EPSAccToken);
            HomeHelper.ClearCache();
          }
        }
        else if (func == "Save")
        {
          HomeHelper.SaveCache();
        }
        else if (func == "Delete")
        {
          EPSHelper.DeleteDraftWorkItem(SessionHandler.WorkItemId, SessionHandler.EPSAccToken);
          HomeHelper.ClearCache();
        }
      }
      catch (Exception ex)
      {
        // Message consist of tab title and message text
        string[] msg = ex.Message.Split('|');
        // Return the result
        return new JsonResult
        {
          JsonRequestBehavior = JsonRequestBehavior.AllowGet,
          Data = new
          {
            title = msg.Length > 1 ? msg[0] : string.Empty,
            message = msg.Length > 1 ? msg[1] : ex.Message,
            isValid = false
          }
        };
      }
      // Return the result
      return new JsonResult
      {
        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        Data = new { title = string.Empty, message = string.Empty, isValid = true }
      };

    }

    public string UpdateCache()
    {
      var collection = new FormCollection(Request.QueryString);
      // Format collection for problem fields liked number and dates
      if (collection["model"] != null)
      {
        Helpers.UtilityHelper.FormatCollectionForModel(collection);
        if (collection["model"].Equals(typeof(CaseData).Name))
        {
          CaseData cdata = CaseDataHelper.CaseData;
          TryUpdateModel(cdata, collection.ToValueProvider());
        }
        else if (collection["model"].Equals(typeof(MRSArchisWS.document).Name))
        {
          var at = DocumentHelper.GetCacheObject(collection["cacheId"]);
          TryUpdateModel(at, collection.ToValueProvider());
        }
        else if (collection["model"].Equals(typeof(Models.Checklist.CheckList).Name))
        {
          Models.Checklist.CheckList checklist = CheckListHelper.GetCacheObject(collection["cacheid"]);
          if (checklist != null && checklist.WorkflowStepName == SessionHandler.CurrentStep)
            checklist.isDirty = CheckListHelper.UpdateCheckListAnswers(checklist.Questions, collection);
        }

      }
      return "";
    }

  }
}
