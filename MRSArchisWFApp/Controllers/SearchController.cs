﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MRSArchisWFApp.Helpers;

namespace MRSArchisWFApp.Controllers
{
  public class SearchController : Controller
  {
    public ActionResult Index(string mainType, string documentTypeId, string searchTerm, string returnUrl)
    {
      return View();
    }

    public ActionResult Results(string mainType, string documentTypeId, string searchTerm, string returnUrl)
    {
      try
      {
        ViewBag.searchTerm = searchTerm;
        ViewBag.returnUrl = returnUrl;
        return View(SessionHandler.CSHelperObject.SearchCaseForDocumentType(
          Helpers.MRSArchis.CaseHelper.GetBaseCaseNumber(SessionHandler.CaseNumber), mainType, documentTypeId, searchTerm));
      }
      catch (Exception ex)
      {
        ViewBag.message = ex.Message;
      }
      return View("Error");
    }
  }
}
