﻿using MRSArchisWFApp.CHOService;
using MRSArchisWFApp.Helpers.EPS;
using MRSArchisWFApp.Helpers.MRSArchis;
using MRSArchisWFApp.Models.EPS;
using System.Collections.Generic;
using System.Linq;

namespace MRSArchisWFApp.Helpers.CHO
{
    public class MonumentHelper : CachedDataHelper<rijksmonument>
    {
        public static List<rijksmonument> Monuments
        {
            get
            {
                if (CacheData == null || !HasSession)
                {
                    TrackSession();
                    LoadMonumentsFromDb();
                }
                return CacheData != null ? CacheData.ToList() : new List<rijksmonument>();
            }
        }

        private static void LoadMonumentsFromDb()
        {
            CaseData cdata = CaseDataHelper.CaseData;
            if (cdata.caseTypeId.HasValue)
            {
                CHOServiceClient choService = new CHOServiceClient();

                var zaakId = CaseHelper.GetCaseId(cdata.caseNumber).GetValueOrDefault();
                var monuments = choService.findRijksmonumentByZaak(zaakId);
                                
                if (monuments != null)
                {
                    foreach (var monument in monuments)
                    {
                        // Todo: Maybe change this to load correct stateGeometry depending on WorkStep
                        var stateGeometryId = StatesGeometryHelper.getStateGeometryId(StatesGeometryHelper.voorbeschermingsDico01);
                        monument.geometrie = LoadGeometriesFromDb(choService, monument.choId).SingleOrDefault(g => g.statusGeometrieId == stateGeometryId); //.FirstOrDefault(); 

                        AddToCache(monument);                        
                    }
                }
            }
        }
        
        private static List<locatieOmschrijving> LoadLocationDescriptionsFromDb(CHOServiceClient choService, long choId)
        {
            var locationDescriptions = choService.findLocatieOmschrijving(choId);

            return locationDescriptions != null ? locationDescriptions.ToList() : new List<locatieOmschrijving>();
        }

        private static List<geometrie> LoadGeometriesFromDb(CHOServiceClient choService, long choId)
        {
            var geometries = choService.findGeometrie(choId);

            return geometries != null ? geometries.ToList() : new List<geometrie>();
        }

        new public static void SaveToDB()
        {
            if (CacheData != null && CacheData.Count() > 0)
            {
                CaseData cdata = CaseDataHelper.CaseData;
                if (!string.IsNullOrEmpty(cdata.caseNumber))
                {
                    CHOServiceClient choService = new CHOServiceClient();

                    foreach (var monument in CacheData)
                    {
                        if (monument.geometrie.isDirty)
                        {
                            monument.geometrie.geometrieId = choService.insertOrUpdateGeometrie(SessionHandler.Username, monument.geometrie);
                            monument.geometrie.geometrieIdSpecified = true;

                            monument.geometrie.isDirty = false;
                        }                        
                    }
                }
            }

            if (CacheDeleted != null && CacheDeleted.Count() > 0)
            {
                CaseData cdata = CaseDataHelper.CaseData;
                if (!string.IsNullOrEmpty(cdata.caseNumber))
                {
                    CHOServiceClient choService = new CHOServiceClient();

                    foreach (var monument in CacheDeleted)
                    {
                        // TODO: Delete service/function needs te be implemented
                        //choService.deleteRijksmonument(monument.choId);
                    }
                }
            }
        }
    }
}