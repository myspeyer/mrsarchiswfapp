﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MRSArchisWFApp.CHOService;

namespace MRSArchisWFApp.Helpers.CHO
{
    public class OriginsHelper : CachedDataHelper<herkomst>
    {
        public static List<herkomst> Origins
        {
            get
            {
                if (CacheData == null || !HasSession)
                {
                    TrackSession();
                    LoadFromDb();
                }
                return CacheData != null ? CacheData.ToList() : new List<herkomst>();
            }
        }

        private static void LoadFromDb()
        {
            CHOServiceClient choService = new CHOServiceClient();

            var origins = choService.findHerkomst(DateTime.Now);
            if (origins != null)
            {
                foreach (var origin in origins)
                {
                    AddToCache(origin);
                }
            }
        }
    }
}