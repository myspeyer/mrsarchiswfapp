﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MRSArchisWFApp.CHOService;

namespace MRSArchisWFApp.Helpers.CHO
{
    public class QualitiesGeometryHelper : CachedDataHelper<kwaliteitGeometrie>
    {
        public static List<kwaliteitGeometrie> Qualities
        {
            get
            {
                if (CacheData == null || !HasSession)
                {
                    TrackSession();
                    LoadFromDb();
                }
                return CacheData != null ? CacheData.ToList() : new List<kwaliteitGeometrie>();
            }
        }

        private static void LoadFromDb()
        {
            CHOServiceClient choClient = new CHOServiceClient();

            var qualities = choClient.findKwaliteitGeometrie(DateTime.Now);
            if (qualities != null)
            {
                foreach (var kwaliteitGeometrie in qualities)
                {
                    AddToCache(kwaliteitGeometrie);
                }
            }
        }
    }
}