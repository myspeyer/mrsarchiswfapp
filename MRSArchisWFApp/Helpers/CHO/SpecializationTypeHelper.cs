﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MRSArchisWFApp.CHOService;

namespace MRSArchisWFApp.Helpers.CHO
{
    public class SpecializationTypeHelper : CachedDataHelper<specialisatieType>
    {
        public const string monument = "Monument"; // TODO: Move to config.

        public static List<specialisatieType> Specializations
        {
            get
            {
                if (CacheData == null || !HasSession)
                {
                    TrackSession();
                    LoadFromDb();
                }
                return CacheData != null ? CacheData.ToList() : new List<specialisatieType>();
            }
        }

        private static void LoadFromDb()
        {
            CHOServiceClient choClient = new CHOServiceClient();

            var specializations = choClient.findSpecialisatieType(DateTime.Now);
            if (specializations != null)
            {
                foreach (var kwaliteitGeometrie in specializations)
                {
                    AddToCache(kwaliteitGeometrie);
                }
            }
        }

        public static long getId(string value)
        {
            return Specializations.Single(st => st.naam == value).specialisatieTypeId;
        }
    }
}