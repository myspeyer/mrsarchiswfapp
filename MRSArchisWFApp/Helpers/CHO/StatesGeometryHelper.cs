﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MRSArchisWFApp.CHOService;

namespace MRSArchisWFApp.Helpers.CHO
{
    public class StatesGeometryHelper : CachedDataHelper<statusGeometrie>
    {
        public const string voorbeschermingsDico01 = "voorbeschermingsdico 0.1"; // TODO: Move to web.config 

        public static List<statusGeometrie> States
        {
            get
            {
                if (CacheData == null || !HasSession)
                {
                    TrackSession();
                    LoadFromDb();
                }
                return CacheData != null ? CacheData.ToList() : new List<statusGeometrie>();
            }
        }
                
        private static void LoadFromDb()
        {
            CHOServiceClient choClient = new CHOServiceClient();

            var states = choClient.findStatusGeometrie(DateTime.Now);
            if (states != null)
            {
                //var stateGeometry = new statusGeometrie();
                //stateGeometry.waarde = "";
                //stateGeometry.statusGeometrieId = -1;
                //AddToCache(stateGeometry);

                foreach (var statusGeometrie in states)
                {
                    AddToCache(statusGeometrie);
                }
            }
        }

        public static long getStateGeometryId(string value)
        {
            return States.Single(st => st.waarde == value).statusGeometrieId;
        }

        public static string getStateGeometryWaarde(long value)
        {
            return States.Single(st => st.statusGeometrieId == value).waarde;
        }
    }
}