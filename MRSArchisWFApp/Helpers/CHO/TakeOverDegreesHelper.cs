﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MRSArchisWFApp.CHOService;

namespace MRSArchisWFApp.Helpers.CHO
{
    public class TakeOverDegreesHelper : CachedDataHelper<mateOvername>
    {
        public static List<mateOvername> TakeOverDegrees
        {
            get
            {
                if (CacheData == null || !HasSession)
                {
                    TrackSession();
                    LoadFromDb();
                }
                return CacheData != null ? CacheData.ToList() : new List<mateOvername>();
            }
        }

        private static void LoadFromDb()
        {
            CHOServiceClient choClient = new CHOServiceClient();

            var takeOverDegrees = choClient.findMateOvername(DateTime.Now);
            if (takeOverDegrees != null)
            {
                foreach (var mateOvername in takeOverDegrees)
                {
                    AddToCache(mateOvername);
                }
            }
        }
    }
}