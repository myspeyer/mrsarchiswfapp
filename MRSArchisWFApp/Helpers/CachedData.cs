﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRSArchisWFApp.Helpers
{
  public abstract class CachedData: ICachedData
  {
    [System.Xml.Serialization.XmlIgnore()]
    public string cacheId { get; set; }

    [System.Xml.Serialization.XmlIgnore()]
    public string sessionId { get; set; }

    [System.Xml.Serialization.XmlIgnore()]
    public string workItemId { get; set; }

    [System.Xml.Serialization.XmlIgnore()]
    public string currentStep { get; set; }

    [System.Xml.Serialization.XmlIgnore()]
    public bool isDirty { get; set; }

    [System.Xml.Serialization.XmlIgnore()]
    public bool isNew { get; set; }

    [System.Xml.Serialization.XmlIgnore()]
    public bool isDeleted { get; set; }

    [System.Xml.Serialization.XmlIgnore()]
    public bool isReadOnly { get; set; }
  }
}