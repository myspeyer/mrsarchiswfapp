﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRSArchisWFApp.Helpers
{
  public abstract class CachedDataHelper<T>
  {
    #region log4net
    // Create a logger for this class
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    #endregion

    #region private fields
    // Create the lists for the cache
    private static List<T> _cache = null;
    private static List<string> _sessionList = null;
    #endregion

    #region Cache Methods, no override
    public static T AddToCache(object obj)
    {
      ICachedData _cacheObj = (obj as ICachedData);
      if (_cache == null || !_cache.Exists(item =>
        _cacheObj.sessionId == SessionHandler.SessionId &&
        _cacheObj.workItemId == SessionHandler.WorkItemId &&
        _cacheObj.currentStep == SessionHandler.CurrentStep))
      {
        if (_cache == null)
          _cache = new List<T>();
        _cacheObj.cacheId = Guid.NewGuid().ToString();
        _cacheObj.sessionId = SessionHandler.SessionId;
        _cacheObj.workItemId = SessionHandler.WorkItemId;
        _cacheObj.currentStep = SessionHandler.CurrentStep;
        _cache.Add((T)obj);

        LogHelper.LogDebugInfo("CachedDataHelper.AddToCache", obj, log);
      }
      return (T)obj;
    }

    /// <summary>
    /// For session tracking
    /// </summary>
    public static void TrackSession()
    {
      if (_sessionList == null)
        _sessionList = new List<string>();
      // Add this session so we know we have cached data for it
      if (SessionHandler.SessionId != null && !_sessionList.Exists(s => s == SessionHandler.SessionId))
        _sessionList.Add(SessionHandler.SessionId);
    }

    public static bool HasSession
    {
      get { return _sessionList != null && _sessionList.Exists(s => s == SessionHandler.SessionId); }
    }

    public static List<T> CacheData
    {
      get
      {
        if (_cache != null)
          return _cache.Where(item =>
          (item as ICachedData).sessionId == SessionHandler.SessionId &&
          (item as ICachedData).workItemId == SessionHandler.WorkItemId &&
          (item as ICachedData).currentStep == SessionHandler.CurrentStep &&
          !(item as ICachedData).isDeleted).ToList();
        return null;
      }
    }

    public static List<T> CacheDeleted
    {
      get
      {
        if (_cache != null)
          return _cache.Where(item =>
          (item as ICachedData).sessionId == SessionHandler.SessionId &&
          (item as ICachedData).workItemId == SessionHandler.WorkItemId &&
          (item as ICachedData).currentStep == SessionHandler.CurrentStep &&
          (item as ICachedData).isDeleted).ToList();
        return null;
      }
    }

    public static T GetCacheObject(string id)
    {
      if (_cache != null)
        return _cache.FirstOrDefault(item => (item as ICachedData).cacheId == id);
      return default(T);
    }

    public static T Create()
    {
      // Create new object and it to the cache
      T newObject = (T)Activator.CreateInstance(typeof(T));
      (newObject as ICachedData).isNew = true;
      return AddToCache(newObject);
    }

    public static void Delete()
    {
      if (_cache != null)
        _cache.RemoveAll(item =>
          (item as ICachedData).sessionId == SessionHandler.SessionId &&
          (item as ICachedData).isDeleted);
    }

    public static void Delete(string id)
    {
      // Mark cache item as deleted
      T obj = GetCacheObject(id);
      if (obj != null)
        (obj as ICachedData).isDeleted = true;
    }

    private static void Update(T obj)
    {
      ICachedData _cacheObj = (obj as ICachedData);
      if (_cacheObj.isDirty)
      {
        T cachedObj = _cache.FirstOrDefault(item =>
            (item as ICachedData).sessionId == _cacheObj.sessionId &&
            (item as ICachedData).workItemId == _cacheObj.workItemId &&
            (item as ICachedData).currentStep == _cacheObj.currentStep);
        if (cachedObj != null)
        {
          cachedObj = obj;
        }
        else
        {
          LogHelper.LogDebugInfo("CachedDataHelper.Update", obj, log);
          throw new Exception(_cacheObj.GetType().FullName + ": gegevens in cache niet aanwezig.");
        }
      }
    }

    public static void Clear()
    {
      if (_cache != null)
        _cache.RemoveAll(item => (item as ICachedData).sessionId == SessionHandler.SessionId);
      if (_sessionList != null)
        _sessionList.RemoveAll(s => s == SessionHandler.SessionId);
    }

    public static void Reset()
    {
      if (_cache != null)
      {
        foreach (T t in _cache.Where(item =>
            (item as ICachedData).sessionId == SessionHandler.SessionId &&
            (item as ICachedData).workItemId == SessionHandler.WorkItemId &&
            (item as ICachedData).currentStep == SessionHandler.CurrentStep &&
            ((item as ICachedData).isDirty) || (item as ICachedData).isNew))
        {
          (t as ICachedData).isDirty = false;
          (t as ICachedData).isNew = false;
        }
        _cache.RemoveAll(item => (item as ICachedData).isDeleted);
      }
    }
    #endregion

    #region Methods to be overridden by implementation
    public static void SaveToDB()
    {
      throw new Exception("SaveToDB() not implemented for " + typeof(T).FullName);
    }

    public static bool Validate()
    {
      throw new Exception("Validate() not implemented for " + typeof(T).FullName);
    }
    #endregion
  }
}