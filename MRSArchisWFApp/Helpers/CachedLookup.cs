﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRSArchisWFApp.Helpers
{
  public class CachedLookup
  {
    [System.Xml.Serialization.XmlIgnore()]
    public string label { get; set; }

    [System.Xml.Serialization.XmlIgnore()]
    public bool isDirty { get; set; }

    [System.Xml.Serialization.XmlIgnore()]
    public DateTime lastUpdated { get; set; }

    [System.Xml.Serialization.XmlIgnore()]
    public int refreshInterval { get; set; }
  }
}