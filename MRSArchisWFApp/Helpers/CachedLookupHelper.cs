﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRSArchisWFApp.Helpers
{
  public abstract class CachedLookupHelper<T>
  {
    #region log4net
    // Create a logger for this class
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    #endregion

    #region private fields
    // Create the lists for the cache
    private static List<T> _cache = null;
    #endregion

    #region Cache Methods, no override
    public static T AddToCache(object obj, string label)
    {
      CachedLookup _cacheObj = (obj as CachedLookup);
      if (_cache == null || !_cache.Exists(item => _cacheObj.label == label))
      {
        if (_cache == null)
          _cache = new List<T>();
        _cacheObj.label = label;
        _cacheObj.lastUpdated = DateTime.Now;
        _cache.Add((T)obj);

        // Do some logging
        if (log.IsDebugEnabled) log.DebugFormat("CachedLookupHelper.AddToCache({0}, {1}): SessionId:{2}, WorkitemId:{3}, CurrentStep:{4]",
          obj.GetType().FullName, label, SessionHandler.SessionId, SessionHandler.WorkItemId, SessionHandler.CurrentStep);
      }
      return (T)obj;
    }


    public static T GetCacheObject(string label)
    {
      if (_cache != null)
      {
        T obj = _cache.FirstOrDefault(item => (item as CachedLookup).label == label);
        if (obj != null && (DateTime.Now - (obj as CachedLookup).lastUpdated).Seconds < (obj as CachedLookup).refreshInterval)
          return obj;
        else
          _cache.Remove(obj);
      }
      return default(T);
    }

    #endregion

  }
}