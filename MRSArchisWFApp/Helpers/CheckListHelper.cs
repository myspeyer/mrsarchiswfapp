﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;
using MRSArchis.OpenText.CS;
using MRSArchisWFApp.Models.Checklist;

namespace MRSArchisWFApp.Helpers
{
  public class CheckListHelper : CachedDataHelper<CheckList>
  {
    // Create a logger for this class
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    private const string _checkListDocName = "checklijst";
    private const string _checkListFolderNickname = "Lijsten_MRSArchis";

    public static List<CheckList> CheckLists
    {
      get
      {
        if (CacheData == null || !HasSession)
        {
          TrackSession();

          // Read the checklists for te current case year name
          Models.EPS.CaseData cdata = Helpers.EPS.CaseDataHelper.CaseData;

          // Create the serializer for the checklist
          XmlSerializer deserializer = new XmlSerializer(typeof(List<CheckList>), new XmlRootAttribute("CheckLists"));

          List<CheckList> checkLists = null;

          // Try to get the checklist from the case first 
          MemoryStream ms = SessionHandler.CSHelperObject.GetCaseDocument(cdata.caseNumber.Substring(0, 7), 
            _checkListDocName + "_" + cdata.caseTypeName.ToLower());
          if (ms != null)
            checkLists = (List<CheckList>)deserializer.Deserialize(ms);
          else if (!string.IsNullOrEmpty(cdata.caseTypeName))
          {
            log.Debug("GetFileFromFolder");
            ms = SessionHandler.CSHelperObject.GetFileFromFolder(_checkListFolderNickname, cdata.caseTypeName, 
              _checkListDocName + "_" + cdata.caseTypeName.ToLower());
            if (ms != null)
              checkLists = (List<CheckList>)deserializer.Deserialize(ms);
          }

          // Add to cache
          if (checkLists != null)
            foreach (CheckList cl in checkLists)
              AddToCache(cl);
          else
            AddToCache(new CheckList { Questions = new List<Question> () });
        }
        return CacheData;
      }
    }

    public static bool UpdateCheckListAnswers(List<Question> questions, FormCollection collection)
    {
      bool isDirty = CacheData.Exists(c => c.isDirty);
      if (questions != null)
        foreach (Question q in questions)
        {
          Answer answer;
          if (q.Type == QuestionType.YesNo || q.Type == QuestionType.YesNoNotApplicable)
          {
            if (collection[q.Code] == null)
              answer = Answer.NotYetDecided;
            else
              answer = (Answer)Enum.Parse(typeof(Answer), collection[q.Code]);
            if (q.Answer != answer)
              isDirty = true;
            q.Answer = answer;
          }
          else if (q.Type == QuestionType.Checkbox)
          {
            answer = collection[q.Code] == "true,false" ? Answer.Checked : Answer.Unchecked;
            if (q.Answer != answer)
              isDirty = true;
            q.Answer = answer;
          }

          //Recurse if there are sub questions
          if (UpdateCheckListAnswers(q.SubQuestions, collection))
            isDirty = true;
        }
      return isDirty;
    }

    new public static void SaveToDB()
    {
      // Check if we need to save anything
      if (CacheData != null)
        if (CacheData.Exists(c => c.isDirty))
        {
          // Save the data in the case root folder
          XmlSerializer ser = new XmlSerializer(typeof(List<CheckList>), new XmlRootAttribute("CheckLists"));
          MemoryStream ms = new MemoryStream();
          using (TextWriter writer = new StreamWriter(ms))
          {
            ser.Serialize(writer, CacheData);

            // Upload the checklists to the case folder root
            Models.EPS.CaseData cdata = Helpers.EPS.CaseDataHelper.CaseData;
            ms.Position = 0;
            SessionHandler.CSHelperObject.Upload(
              SessionHandler.CSHelperObject.GetCaseFolder(cdata.caseNumber.Substring(0, 7)), ms, _checkListDocName + "_" + cdata.caseTypeName.ToLower());

            writer.Close();
          }
          LogHelper.LogDebugInfo("SaveToDB", CacheData, log);

          // Reset all dirty flags
          Reset();
        }
    }

    new public static bool Validate()
    {
      if (CacheData != null)
      {
        CheckList checklist = CacheData.FirstOrDefault(c => c.WorkflowStepName == SessionHandler.CurrentStep);
        if (checklist != null)
        {
          // Only check top level questions
          List<Question> questions = checklist.Questions;
          if (questions.Exists(
            q => (q.Type == QuestionType.YesNo || q.Type == QuestionType.YesNoNotApplicable) &&
            q.Answer == Answer.NotYetDecided))
            throw new Exception(string.Format("Vragenlijst '{0}' is niet volledig ingevuld.", checklist.Name));
        }
      }
      return true;
    }
  }
}