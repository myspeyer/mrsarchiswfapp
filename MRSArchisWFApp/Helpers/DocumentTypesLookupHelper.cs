﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MRSArchis.OpenText;
using MRSArchisWFApp.Models;

namespace MRSArchisWFApp.Helpers
{
  public class DocumentTypesLookupList : CachedLookup
  {
    public List<SelectListItem> DocumentTypes { get; set; }
  }
  public class DocumentTypesLookupHelper : CachedLookupHelper<DocumentTypesLookupList>
  {
    private const string _rceDIVDocTypesNickname = "RCE_MRS_DocTypes";


    public static List<SelectListItem> GetLookupList()
    {
      return GetLookupList(SessionHandler.CSHelperObject);
    }

    public static List<SelectListItem> GetLookupList(CSHelper csHelper)
    {
      DocumentTypesLookupList documentTypesLookupList = GetCacheObject("doctypes");
      if (documentTypesLookupList == null)
      {
        documentTypesLookupList = new DocumentTypesLookupList { refreshInterval = 1800 };
        using (MemoryStream ms = csHelper.GetDocumentByNickname(_rceDIVDocTypesNickname))
        {
          if (ms != null)
          {
            ms.Position = 0;
            var sr = new StreamReader(ms);
            var Lines = sr.ReadToEnd().Split('\n');
            documentTypesLookupList.DocumentTypes =
              (from string line in Lines
               let x = line.Split(';')
               orderby x[1], x[2]
               select new SelectListItem
               {
                 Value = int.Parse(x[0]).ToString(),
                 Text = x[1].Trim() + ";" + x[2].Trim()
               }).ToList();
            AddToCache(documentTypesLookupList, "doctypes");
          }
          else
            documentTypesLookupList.DocumentTypes = new List<SelectListItem> { 
              new SelectListItem { Value = "0", Text = "Overige,Overige (geen specificatie)" } };
        }
      }
      return documentTypesLookupList.DocumentTypes;
    }

    public static string GetDocumentMainType(int id)
    {
      SelectListItem item = GetLookupList(SessionHandler.CSHelperObject).FirstOrDefault(s => s.Value == id.ToString());
      return item != null && !string.IsNullOrEmpty(item.Text) && item.Text.Split(';').Count() > 0 ?
          item.Text.Split(';')[0] : "Overige";
    }

    public static List<string> GetDocumentMainTypes
    {
      get
      {
        return
          (from SelectListItem item in GetLookupList(SessionHandler.CSHelperObject)
           select item.Text.Split(';')[0]
           ).Distinct().ToList();
      }
    }

    public static List<SelectListItem> GetDocumentSubTypesFor(string mainType)
    {
      return
        (from SelectListItem item in GetLookupList(SessionHandler.CSHelperObject).Where(d => d.Text.StartsWith(mainType + ";"))
         select new SelectListItem
         {
           Value = item.Value,
           Text = item.Text.Split(';')[1]
         }).ToList();
    }

    public static string GetDocumentSubTypeFor(int id)
    {
      return GetDocumentSubTypeFor(SessionHandler.CSHelperObject, id);
    }

    public static string GetDocumentSubTypeFor(CSHelper csHelper, int id)
    {
      SelectListItem item = GetLookupList(csHelper).FirstOrDefault(s => s.Value == id.ToString());
      return item != null && !string.IsNullOrEmpty(item.Text) && item.Text.Split(';').Count() > 1 ?
          item.Text.Split(';')[1] : "Overige (geen specificatie)";
    }

    public static string ExtractSubType(string s)
    {
      return !string.IsNullOrEmpty(s) && s.Split(';').Count() > 1 ? s.Split(';')[1] : "Overige (geen specificatie)";
    }

    public static string ExtractMainType(string s)
    {
      return !string.IsNullOrEmpty(s) && s.Split(';').Count() > 0 ? s.Split(';')[0] : "Overige";
    }
  }
}