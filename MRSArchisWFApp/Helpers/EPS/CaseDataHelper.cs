﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MRSArchis.OpenText.EPS;
using MRSArchis.OpenText;
using MRSArchisWFApp.Helpers.MRSArchis;
using MRSArchisWFApp.Models.EPS;

namespace MRSArchisWFApp.Helpers.EPS
{
  public class CaseDataHelper : CachedDataHelper<CaseData>
  {
    #region log4net
    // Create a logger for this class
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    #endregion

    public static CaseData CaseData
    {
      get
      {
        if (CacheData == null || CacheData.Count() == 0)
        {
          // Create a new object for the case info
          CaseData cdata = new CaseData();

          // Could be null if session expired or invalid workitem id
          if (EPSHelper.GetWorkItemData(SessionHandler.WorkItemId, cdata) == null)
          {
            string errorMsg = "Kan de gegevens van de workflow niet ophalen. Start de taak opnieuw op of log opnieuw in.";
            if (log.IsDebugEnabled) log.Debug(string.Format("{0} WorkItemId:{1}", errorMsg, SessionHandler.WorkItemId));
            throw new Exception(errorMsg);
          }
          cdata.isDirty = false;

          // If the session has the case number use it to initialize
          if (string.IsNullOrEmpty(cdata.caseNumber) && !string.IsNullOrEmpty(SessionHandler.CaseNumber))
            cdata.caseNumber = SessionHandler.CaseNumber;

          // If the workflow has the role, case type and status type set the id's here
          if (!string.IsNullOrEmpty(cdata.roleTypeName))
          {
            cdata.roleTypeId = CaseHelper.GetRoleTypeId(cdata.roleTypeName);
            if (!string.IsNullOrEmpty(cdata.caseTypeName))
            {
              cdata.caseTypeId = CaseHelper.GetCaseTypeId(cdata.caseTypeName, cdata.roleTypeId);
              if (!string.IsNullOrEmpty(cdata.statusTypeName))
                cdata.statusTypeId = CaseHelper.GetStatusTypeId(cdata.statusTypeName);
            }
          }
          if (!string.IsNullOrEmpty(cdata.caseNumber))
          {
            // Update sampling, completion and fitting status from the node metadata 
            var cdataList = SessionHandler.CSHelperObject.BindNodeMetadata(typeof(CaseData),
                CSHelper.caseNicknamePrefix + Helpers.MRSArchis.CaseHelper.GetBaseCaseNumber(cdata.caseNumber)).ConvertAll(o => (CaseData)o);

            // Initialize case data with CS case metadata 
            var cdataCS = cdataList != null ? cdataList.FirstOrDefault() : null;
            if (cdataCS != null)
            {
              cdata.inProgress = cdataCS.inProgress;
              cdata.notification = cdataCS.notification;
              // Only for subcases initialize the region
              if (string.Compare(cdata.caseTypeName, Models.CaseService.CaseTypes.RCE_START_ZAAK.ToString(), true) != 0)
                cdata.region = cdataCS.region;
            }

            // Initialize the case data with MRSArhis case data (note final logging is done in CacheDataHelper)

            // Initialize case data with CS metadata
            List<object> csObjects = SessionHandler.CSHelperObject.BindNodeMetadata(typeof(Models.CS.CaseDetail), 
              CSHelper.caseNicknamePrefix + cdata.caseNumber);
            var caseDetailCS = (Models.CS.CaseDetail)csObjects.FirstOrDefault();
            if (caseDetailCS != null)
            {
              cdata.registrationDate = caseDetailCS.registrationDate ?? cdata.registrationDate;
              cdata.receiptDate = caseDetailCS.receiptDate ?? cdata.receiptDate;
              cdata.isDigital = caseDetailCS.digital;
            }
            if (log.IsDebugEnabled) log.DebugFormat("CSHelper.BindNodeMetadata:\n\t {0}", cdata);
          }
          AddToCache(cdata);
        }
        // Should be just one object in the cache list, so return it
        return CacheData.First();
      }
    }
    /// <summary>
    /// Save the active case data to EPS and to CS case detail metadata
    /// </summary>
    /// <remarks>
    /// No need to update SACE case cache since it was already updated on subsidy creation
    /// </remarks>
    new public static void SaveToDB()
    {
      if (CacheData != null && CacheData.Count() > 0)
      {
        CaseData cdata = CaseData;


        if (cdata != null && cdata.isDirty)
        {
          if (!string.IsNullOrEmpty(cdata.caseNumber))
          {
            // Sampling, fitting status, nottification may have changed retrieve them again
            var cdataList = SessionHandler.CSHelperObject.BindNodeMetadata(typeof(CaseData),
                CSHelper.caseNicknamePrefix + Helpers.MRSArchis.CaseHelper.GetBaseCaseNumber(cdata.caseNumber)).ConvertAll(o => (CaseData)o);

            // Re-initalize the case data with the CS case metadata 
            var cdataCS = cdataList != null ? cdataList.FirstOrDefault() : null;
            if (cdataCS != null)
            {
              cdata.notification = cdataCS.notification;
              if (string.IsNullOrEmpty(cdata.region))
                cdata.region = cdataCS.region;
            }
            // Update EPS and CS
            EPSHelper.UpdateProcessInstanceData(SessionHandler.ProcessInstanceId, cdata);
            SessionHandler.CSHelperObject.UpdateNodeMetadata(CacheData.ToArray(),
              CSHelper.caseNicknamePrefix + Helpers.MRSArchis.CaseHelper.GetBaseCaseNumber(cdata.caseNumber));
          }

          // Update the case detail, for analog case update the CS case detail with registration scan receipt data
          Models.CS.CaseDetail caseDetail = null;
          List<object> csObjects;
          switch (SessionHandler.CurrentStep)
          {
            case "initiator_ZR_nz":
              // Set the scan metadata
              caseDetail = new Models.CS.CaseDetail()
              {
                receiptDate = cdata.receiptDate,
                registrationDate = cdata.registrationDate,
                caseType = cdata.caseTypeName.ToLower()
              };
              SessionHandler.CSHelperObject.UpdateNodeMetadata(new object[] { caseDetail }, CSHelper.caseNicknamePrefix + cdata.caseNumber);
              break;

            default:
              csObjects = SessionHandler.CSHelperObject.BindNodeMetadata(typeof(Models.CS.CaseDetail), CSHelper.caseNicknamePrefix + cdata.caseNumber);
              caseDetail = (Models.CS.CaseDetail)csObjects.FirstOrDefault();
              if (caseDetail != null)
              {
                caseDetail.caseType = cdata.caseTypeName;
                if (caseDetail.isDirty)
                  SessionHandler.CSHelperObject.UpdateNodeMetadata(new object[] { caseDetail }, CSHelper.caseNicknamePrefix + cdata.caseNumber);
              }
              break;
          }
          cdata.isDirty = false;

          LogHelper.LogDebugInfo("SaveToDB", cdata, log);
        }
      }
    }

    new public static bool Validate()
    {
      bool isValid = true;
      string errorMsg = "";
      CaseData cdata = CaseData;
      switch (SessionHandler.CurrentStep)
      {
        case "initiator_ZR_nz":
          isValid = !string.IsNullOrEmpty(cdata.caseNumber) 
            && cdata.registrationDate >= new DateTime(1990, 1, 1) 
            && cdata.receiptDate >= new DateTime(1990, 1, 1)
            && !string.IsNullOrEmpty(cdata.targetCaseTypeName);
          errorMsg = "Registratie|" +  (string.IsNullOrEmpty(cdata.targetCaseTypeName) ? 
            "Status voor vervolgbehandeling ontbreekt" : "Zaaknummer en/of datumgegevens zijn onvolledig");
          break;

        case "user_MI_in":
          isValid = !string.IsNullOrEmpty(cdata.caseTypeName) && 
            (cdata.caseTypeName.Equals("Aanwijzen monument") || cdata.caseTypeName.Equals("Wijzigen monument") ||
            cdata.caseTypeName.Equals("Afvoeren monument"));
          errorMsg = "Intake|Onjuist zaatype, kies een zaaktype voor verdere afhandeling";
          break;

      }
      if (!isValid)
        throw new Exception(errorMsg);
      return isValid;
    }
  }
}