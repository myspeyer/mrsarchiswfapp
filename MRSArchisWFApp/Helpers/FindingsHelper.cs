﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Xml.Serialization;
using MRSArchisWFApp.Models.EPS;
using MRSArchisWFApp.Helpers;
using MRSArchisWFApp.Helpers.EPS;
using MRSArchisWFApp.Helpers.MRSArchis;
using MRSArchisWFApp.Models;

namespace MRSArchisWFApp.Helpers 
{
  public class FindingsHelper : CachedDataHelper<Finding>
  {
    #region log4net
    // Create a logger for this class
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    #endregion

    #region private members
    private const string _findingsDocName = "bevindingen";
    #endregion

    #region public static properties and methods
    public static List<Finding> Findings
    {
      get
      {
        if ((CacheData == null || !HasSession) && !string.IsNullOrEmpty(SessionHandler.CaseNumber))
        {
          TrackSession();

          // Read the findings and add them to the cache
          var findings = GetCaseFindings(SessionHandler.CaseNumber);

          // Add to cache
          if (findings != null)
            foreach (Finding f in findings)
              AddToCache(f);
        }
        return CacheData ?? new List<Finding>();
      }
    }

    new public static void SaveToDB()
    {
      // Check if we need to save anything, avoid retrieving any data by directly accessing the cache
      if (CacheData != null)
        if (CacheData.Exists(f => f.isDirty) || CacheDeleted.Count > 0)
        {
          // Save the data in the case root folder
          XmlSerializer ser = new XmlSerializer(typeof(List<Finding>), new XmlRootAttribute("Findings"));
          MemoryStream ms = new MemoryStream();
          using (TextWriter writer = new StreamWriter(ms))
          {
            ser.Serialize(writer, CacheData);

            // Upload the findings to the case folder root
            CaseData cdata = CaseDataHelper.CaseData;
            ms.Position = 0;
            SessionHandler.CSHelperObject.Upload(
              SessionHandler.CSHelperObject.GetCaseFolder(cdata.caseNumber.Substring(0, 7)), ms, _findingsDocName);

            writer.Close();
          }

          LogHelper.LogDebugInfo("SaveToDB", CacheData, log);
          // Reset all dirty flags
          Reset();
        }
    }

    public static List<Finding> GetCaseFindings(string caseNumber)
    {
      // Find the folder in CS
      XmlSerializer deserializer = new XmlSerializer(typeof(List<Finding>), new XmlRootAttribute("Findings"));

      // Try to get the findings from the case 
      MemoryStream ms = SessionHandler.CSHelperObject.GetCaseDocument(CaseHelper.GetBaseCaseNumber(caseNumber), _findingsDocName);
      if (ms != null)
        return (List<Finding>)deserializer.Deserialize(ms);

      return null;
    }

    #endregion  }
  }
}