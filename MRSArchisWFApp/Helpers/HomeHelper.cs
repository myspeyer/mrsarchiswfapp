﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MRSArchis.OpenText;
using MRSArchis.OpenText.EPS;
using MRSArchisWFApp.Helpers;
using MRSArchisWFApp.Helpers.EPS;
using MRSArchisWFApp.Helpers.MRSArchis;
using MRSArchisWFApp.Helpers.CHO;
using MRSArchisWFApp.Models.EPS;

namespace MRSArchisWFApp.Helpers
{
  public class HomeHelper
  {
    // Create a logger for this class 
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public static void SetupSessionContext(string workitemid, string epsacctoken, string isreadonly)
    {
      SessionHandler.CaseNumber = string.Empty;
      // Add the workitem and stepinfo to the cache
      WorkItemInfo workItemInfo = EPSHelper.GetWorkItemInfo(workitemid);
      SessionHandler.WorkItemId = workitemid;


      SessionHandler.CurrentStep = workItemInfo.CurrentStep;

      // Clear the full cache if this is another process
      //if (SessionHandler.ProcessInstanceId != workItemInfo.ProcessInstanceId)
      //  ClearCache();

      SessionHandler.ProcessInstanceId = workItemInfo.ProcessInstanceId;

      // strip out OTDSToken
      SessionHandler.EPSAccToken = epsacctoken.Split(new string[] { "otdsToken=" }, StringSplitOptions.None).First();

      // Store the user credentials
      SessionHandler.Username = workItemInfo.UserName;
      SessionHandler.UserDisplayName = workItemInfo.UserDisplayName;
      SessionHandler.UserEmail = workItemInfo.Email;
      if (string.Compare(ConfigurationManager.AppSettings["CSUseLoginDomain"], "true", true) == 0)
        SessionHandler.LoginDomain = EPSHelper.GetLoginDomain(SessionHandler.EPSAccToken);

      // Capture readonly state in session
      SessionHandler.IsReadonly = isreadonly;

      ClearCache();

      // Try logon to CS before proceeding and save CS Helper object
      CSHelper csHelper = new CSHelper(SessionHandler.Username, SessionHandler.LoginDomain);
      csHelper.CSLogon();
      SessionHandler.CSHelperObject = csHelper;

      //CSHelper.CSLogon();

      // Set the status for application/justification and store active case number in session
      CaseData cdata = CaseDataHelper.CaseData;

      // Initialize the case data workflow data
      //cdata.status = workItemInfo.CurrentStep;
      cdata.emailCaseworker = workItemInfo.Email;

      // Store in session variable
      SessionHandler.CaseNumber = cdata.caseNumber;

    }

    public static void SaveCache()
    {
      string title = String.Empty;
      try
      {
        // Save the cache to the database, update Content Server first
        if (!string.IsNullOrEmpty(SessionHandler.CWSAuthToken))
        {
        }

        title = "Zaak";
        if (log.IsDebugEnabled) log.Debug("Saving " + title);

        // Update SACE at the first possible occassion, but only if application
        CaseData cdata = CaseDataHelper.CaseData;

        // Update EPS
        CaseDataHelper.SaveToDB();

        // Save documents
        title = "Documenten";
        if (log.IsDebugEnabled) log.Debug("Saving " + title);
        DocumentHelper.SaveToDB();

        // Save documents
        title = "Bevindingen";
        if (log.IsDebugEnabled) log.Debug("Saving " + title);
        FindingsHelper.SaveToDB();
          
        title = "Dico";
        if (log.IsDebugEnabled) log.Debug("Saving " + title);
        MonumentHelper.SaveToDB();

        title = "Checklijst";
        if (log.IsDebugEnabled) log.Debug("Saving " + title);
        CheckListHelper.SaveToDB();

      }
      catch (Exception ex)
      {
        throw new Exception(title + "|" + ex.Message);
      }
    }

    public static void ClearCache()
    {
      // Clear the cache
      DocumentHelper.Clear();

      CaseDataHelper.Clear();
    }
  }
}