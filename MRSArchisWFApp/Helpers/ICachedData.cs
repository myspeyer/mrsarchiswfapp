﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MRSArchisWFApp.Helpers
{
  interface ICachedData
  {
    string cacheId { get; set; }

    string sessionId { get; set; }

    string workItemId { get; set; }

    string currentStep { get; set; }

    bool isDirty { get; set; }

    bool isNew { get; set; }

    bool isDeleted { get; set; }

    bool isReadOnly { get; set; }
  }
}
