﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MRSArchis.OpenText.EPS.DataAnnotations;
using MRSArchis.OpenText.CS.DataAnnotations;

namespace MRSArchisWFApp.Helpers
{
  public class LogHelper
  {
    #region Public methods

    public static void LogDebugInfo(string method, object obj, log4net.ILog log)
    {
      // Log the object
      if (log.IsDebugEnabled)
        using (log4net.ThreadContext.Stacks["NDC"].Push(SessionHandler.WorkItemId))
        {
          if (!string.IsNullOrEmpty(method))
            log.DebugFormat("\n\t{0}({1})\n\t[CurrentStep]: {2}\n\t[SessionId]: {3}\n\t[UserId]: {4}\n\t[Data]:\n\t{5}",
              method, obj.GetType().FullName, SessionHandler.CurrentStep, SessionHandler.SessionId, SessionHandler.Username, obj);
          else
            log.DebugFormat("\n\t{0}\n\t[CurrentStep]: {1}\n\t[SessionId]: {2}\n\t[UserId]: {3}\n\t[Data]:\n\t{4}",
              obj.GetType().FullName, SessionHandler.CurrentStep, SessionHandler.SessionId, SessionHandler.Username, obj);
        }
    }

    public static string RenderObject(object obj)
    {
      if (obj == null)
        return "null";

      StringBuilder sb = new StringBuilder();

      PropertyInfo[] props = obj.GetType().GetProperties();
      foreach (PropertyInfo prop in props)
      {
        string typeName = prop.PropertyType.FullName;
        // check for nullable types and nullable array types and set the type name
        if (prop.PropertyType.IsGenericType || prop.PropertyType.IsArray)
          if (prop.PropertyType.FullName == typeof(Nullable<DateTime>).FullName)
            typeName = "System.DateTime?";
          else if (prop.PropertyType.FullName == typeof(Nullable<DateTime>[]).FullName)
            typeName = "System.DateTime?[]";
          else if (prop.PropertyType.FullName == typeof(Nullable<Int16>).FullName)
            typeName = "System.Int16?";
          else if (prop.PropertyType.FullName == typeof(Nullable<Int16>[]).FullName)
            typeName = "System.Int16?[]";
          else if (prop.PropertyType.FullName == typeof(Nullable<Int32>).FullName)
            typeName = "System.Int32?" + (prop.PropertyType.IsArray ? "[]" : "");
          else if (prop.PropertyType.FullName == typeof(Nullable<Int32>[]).FullName)
            typeName = "System.Int32?[]";
          else if (prop.PropertyType.FullName == typeof(Nullable<Decimal>).FullName)
            typeName = "System.Decimal?";
        foreach (Attribute attr in Attribute.GetCustomAttributes(prop))
        {
          string fieldName = string.Empty;
          if (attr.GetType() == typeof(EPSFieldNameAttribute)
            || attr.GetType() == typeof(CSFieldNameAttribute)
            || attr.GetType() == typeof(DisplayNameAttribute))
          {
            fieldName = attr.GetType() == typeof(EPSFieldNameAttribute) ?
              ((EPSFieldNameAttribute)attr).EPSFieldName :
                attr.GetType() == typeof(CSFieldNameAttribute) ? ((CSFieldNameAttribute)attr).FieldName : ((DisplayNameAttribute)attr).DisplayName;
          }
          else
          {
            DisplayNameAttribute displayAttr;

            MetadataTypeAttribute metadataType = (MetadataTypeAttribute)obj.GetType().GetCustomAttributes(typeof(MetadataTypeAttribute), true).FirstOrDefault();
            if (metadataType != null)
            {
              var metadataProperty = metadataType.MetadataClassType.GetProperty(prop.Name);
              if (metadataProperty != null)
              {
                displayAttr = (DisplayNameAttribute)metadataProperty.GetCustomAttributes(typeof(DisplayNameAttribute), true).SingleOrDefault();
                if (attr != null)
                  fieldName = displayAttr.DisplayName;
              }
            }

          }
          if (!string.IsNullOrEmpty(fieldName))
          {
            if (prop.GetValue(obj, null) != null)
            {
              switch (typeName)
              {
                case "System.String":
                  sb.AppendFormat("{0}:{1}", fieldName, (string)prop.GetValue(obj, null));
                  break;

                case "System.String[]":
                  string[] strvals = (string[])prop.GetValue(obj, null);
                  sb.AppendFormat("{0}[{1}]:", fieldName, strvals.Length);
                  if (strvals != null)
                    foreach (string s in strvals)
                      sb.AppendFormat("{0}, ", s);
                  break;

                case "System.Int16":
                case "System.Int32":
                  sb.AppendFormat("{0}:{1}", fieldName, ((int)prop.GetValue(obj, null)));
                  break;

                case "System.Int16[]":
                case "System.Int32[]":
                  int[] ints = (int[])prop.GetValue(obj, null);
                  sb.AppendFormat("{0}[{1}]:", fieldName, ints.Length);
                  foreach (int i in ints)
                    sb.AppendFormat("{0}, ", i);
                  break;

                case "System.Int16?":
                case "System.Int32?":
                  sb.AppendFormat("{0}:{1};", fieldName, ((int?)prop.GetValue(obj, null)));
                  break;

                case "System.Int16?[]":
                case "System.Int32?[]":
                  int?[] nullableInts = (int?[])prop.GetValue(obj, null);
                  sb.AppendFormat("{0}[{1}]:", fieldName, nullableInts.Length);
                  foreach (int? i in nullableInts)
                    sb.AppendFormat("{0}, ", i.HasValue ? i.Value.ToString() : "-");
                  break;

                case "System.Int64":
                  sb.AppendFormat("{0}:{1}", fieldName, ((long)prop.GetValue(obj, null)));
                  break;

                case "System.Boolean":
                  sb.AppendFormat("{0}:{1}", fieldName, ((bool)prop.GetValue(obj, null)));
                  break;

                case "System.DateTime":
                  sb.AppendFormat("{0}:{1}", fieldName, ((DateTime)prop.GetValue(obj, null)).ToString("s"));
                  break;

                case "System.DateTime?":
                  DateTime? nullableDateTime = ((DateTime?)prop.GetValue(obj, null));
                  sb.AppendFormat("{0}:{1}", fieldName,
                    nullableDateTime.HasValue ? nullableDateTime.Value.ToString("s") : String.Empty);
                  break;

                case "System.DateTime[]":
                  DateTime[] dateTimes = (DateTime[])prop.GetValue(obj, null);
                  sb.AppendFormat("{0}[{1}]:", fieldName, dateTimes.Length);
                  foreach (DateTime d in dateTimes)
                    sb.AppendFormat("{0}, ", d.ToString("s"));
                  break;

                case "System.DateTime?[]":
                  DateTime?[] nullableDateTimes = (DateTime?[])prop.GetValue(obj, null);
                  sb.AppendFormat("{0}[{1}]:", fieldName, nullableDateTimes.Length);
                  foreach (DateTime? d in nullableDateTimes)
                    sb.AppendFormat("{0}, ", d.HasValue ? d.Value.ToString("s") : String.Empty);
                  break;

                case "System.Decimal":
                  sb.AppendFormat("{0}:{1}", fieldName, ((Decimal)prop.GetValue(obj, null)));
                  break;

                case "System.Decimal?":
                  Decimal? nullableDecimal = ((Decimal?)prop.GetValue(obj, null));
                  sb.AppendFormat("{0}:{1}", fieldName,
                    nullableDecimal.HasValue ? nullableDecimal.Value.ToString() : String.Empty);
                  break;
              }
              sb.Append("\n\t");
            }
            //else
            //  sb.Append(fieldName + ":;");

            //if (cnt % 4 == 0)
            //  sb.Append("\n\t");
            break;
          }
        }
      }
      return sb.ToString();
    }

    #endregion
  }
}