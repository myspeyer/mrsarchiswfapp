﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MRSArchisWFApp.Models.CaseService;
using MRSArchisWFApp.MRSArchisWS;

namespace MRSArchisWFApp.Helpers.MRSArchis
{
  public class CaseHelper
  {
    #region Constants for case business logic

    private const string _ocwDivision = "0010";
    private const string _caseFutureUse = "00000000000000000000000";

    #endregion

    private static string _rceDepartment
    {
      get { return ConfigurationManager.AppSettings["sequenceservice.afdelingsnummer"]; }
    }
    public static string caseNumberPrefix
    {
      get
      {
        return _ocwDivision + _rceDepartment + _caseFutureUse;
      }
    }


    #region Case business logic

    public static long? GetCaseId(string caseNumber)
    {
      // Get the case id if case number has been provided
      if (!string.IsNullOrEmpty(caseNumber))
      {
        ZaakServiceClient zsClient = new ZaakServiceClient();
        return zsClient.getZaakId(caseNumberPrefix + caseNumber);
      }
      return new int?();
    }

    public static long GetRoleTypeId(string roleName)
    {
      ZaakServiceClient zsClient = new ZaakServiceClient();
      return zsClient.getRoltypeId(roleName);
    }

    public static List<SelectListItem> GetCaseRoleTypes(long? caseTypeId)
    {
      if (caseTypeId.HasValue)
      {
        ZaakServiceClient zsClient = new ZaakServiceClient();
        return (from MRSArchisWS.roltype roletype in zsClient.findRoltypeByZaaktype(caseTypeId.Value)
                select new SelectListItem
                {
                  Value = roletype.id.ToString(),
                  Text = roletype.omschrijving
                }).ToList();
      }
      else
        return new List<SelectListItem>();
    }

    public static long GetStatusTypeId(string statusTypeName)
    {
      ZaakServiceClient zsClient = new ZaakServiceClient();
      return zsClient.getStatustypeId(statusTypeName);
    }

    public static List<SelectListItem> GetStatusTypesForRole(long? roleTypeId)
    {
      if (roleTypeId.HasValue)
      {
        ZaakServiceClient zsClient = new ZaakServiceClient();
        return (from MRSArchisWS.statustype statustype in zsClient.findStatustypeByRoltype(roleTypeId.GetValueOrDefault())
                select new SelectListItem
                {
                  Value = statustype.id.ToString(),
                  Text = statustype.omschrijving
                }).ToList();
      }
      else
        return new List<SelectListItem>();
    }

    public static long? GetCaseTypeId(string caseTypeName, long roleTypeId)
    {
      ZaakServiceClient zsClient = new ZaakServiceClient();
      var caseTypes = zsClient.findZaaktypeByRoltype(roleTypeId);
      var caseType = caseTypes.FirstOrDefault(ct => ct.selectielijstTekst.Equals(caseTypeName, StringComparison.InvariantCultureIgnoreCase));
      if (caseType != null)
        return caseType.id;
      return null;
    }

    public static string CreateCase(
      int? serialnum, 
      DateTime? registrationDate, 
      DateTime? receiptDate, 
      long roleTypeId, 
      long caseTypeId, 
      long statusTypeId, 
      string caseNumber = null)
    {
      long? caseId = CaseHelper.GetCaseId(caseNumber).GetValueOrDefault();

      ZaakServiceClient zsClient = new ZaakServiceClient();

      // Check if employee id exist, if not create a new one
      if (!zsClient.existsMedewerker(SessionHandler.Username))
        zsClient.insertOrUpdateMedewerker(0, SessionHandler.Username, SessionHandler.UserDisplayName, SessionHandler.UserEmail);

      // Use the provided case type
      zaakDetails caseDetails = new zaakDetails
      {
        ontvangstdatum = receiptDate.GetValueOrDefault(),
        ontvangstdatumSpecified = receiptDate.HasValue,
        registratiedatum = registrationDate.GetValueOrDefault(),
        registratiedatumSpecified = registrationDate.HasValue,
        zaaktypeId = caseTypeId,
        roltypeId = roleTypeId,
        statustypeId = statusTypeId,
        opmerking = null
      };
      var caseInfo = zsClient.startZaak(2, caseId, SessionHandler.Username, caseDetails);
      return caseInfo.zaakMetVolgNr;
    }

    public static string MainCaseNumber
    {
      get
      {
        return GetMainCaseNumber(SessionHandler.CaseNumber);
      }
    }


    /// <summary>
    /// Returns the base case number (base number without the sequence number)
    /// </summary>
    /// <param name="caseNumber">The case number for which to retrieve the base case number </param>
    /// <returns>
    /// The 7-digit base case number
    /// </returns>
    public static string GetBaseCaseNumber(string caseNumber)
    {
      return caseNumber.Length >= 10 ? caseNumber.Substring(caseNumber.Length - 10).Substring(0, 7) : null;
    }

    /// <summary>
    /// Returns the main case number (application number)
    /// </summary>
    /// <param name="caseNumber">The case number for which to retrieve the base case number </param>
    /// <returns>
    /// The 10-digit base case number
    /// </returns>
    public static string GetMainCaseNumber(string caseNumber)
    {
      string mainCaseNumber = GetBaseCaseNumber(caseNumber);
      if (caseNumber.StartsWith("0"))
        mainCaseNumber += "001";
      else
        mainCaseNumber += "100";
      return mainCaseNumber;
    }

    public static IEnumerable<SelectListItem> GetRoleList()
    {
      return
        new List<SelectListItem> { 
        new SelectListItem
        {
          Text = "Zaakinitiator",
          Value = "1",
          Selected = false
        },
        new SelectListItem
        {
          Text = "Monumentbeheer",
          Value = "2",
          Selected = false
        },
        new SelectListItem
        {
          Text = "Arheologiebeheer",
          Value = "3",
          Selected = false
        }
      };
    }


    #endregion
  }
}