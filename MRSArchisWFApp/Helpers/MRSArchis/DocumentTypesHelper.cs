﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MRSArchisWFApp.Helpers.EPS;
using MRSArchisWFApp.Models.EPS;
using MRSArchisWFApp.MRSArchisWS;

namespace MRSArchisWFApp.Helpers.MRSArchis
{
  public class DocumentTypesHelper : CachedDataHelper<documenttype>
  {
    #region log4net
    // Create a logger for this class
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    #endregion

    /// <summary>
    /// Get the cached document list
    /// </summary>
    /// <remarks>Return an empty list if no documents were found</remarks>
    /// <remarks>
    /// This is the complete list for the case types and should be filtered
    /// on the status type (when this property becomes available.
    /// </remarks>
    public static List<documenttype> DocumentTypes
    {
      get
      {
        if (CacheData == null || !HasSession)
        {
          TrackSession();
          CaseData cdata = CaseDataHelper.CaseData;
          LoadFromDb(cdata.caseTypeId);
        }
        return CacheData != null ? CacheData.ToList() : new List<documenttype>();
      }
    }

    /// <summary>
    /// Helper funcion for list of main document types
    /// </summary>
    public static List<string> GetDocumentMainTypes
    {
      get
      {
        return
          (from documenttype doctype in DocumentTypes
           select doctype.hoofdDocumenttypeOmschrijving
           ).Distinct().ToList();
      }
    }

    public static List<SelectListItem> GetDocumentSubTypesFor(string mainType)
    {
      return
        (from documenttype doctype in DocumentTypes.Where(d => d.hoofdDocumenttypeOmschrijving.Equals(mainType, StringComparison.CurrentCulture))
         select new SelectListItem
         {
           Value = doctype.id.ToString(),
           Text = doctype.subDocumenttypeOmschrijving
         }).ToList();
    }

    public static string GetDocumentSubTypeFor(int id)
    {
      return DocumentTypes.First(dt => dt.id == id).subDocumenttypeOmschrijving;
    }


    /// <summary>
    /// Load the document list from the database for the case type of the supplied case number
    /// </summary>
    /// <param name="caseNumber">The case number</param>
    private static void LoadFromDb(long? caseTypeId)
    {
      log.Debug("DocumenttypesHelper.LoadFromDb caseTypeId = " + caseTypeId.GetValueOrDefault());
      if (caseTypeId.HasValue)
      {
        ZaakServiceClient zsClient = new ZaakServiceClient();
        var documenttypes = zsClient.findDocumenttypeByZaaktype(caseTypeId.Value);
        if (documenttypes != null)
          foreach (var doctype in documenttypes)
          {
            AddToCache(doctype);
          }
      }
    }
  }
}