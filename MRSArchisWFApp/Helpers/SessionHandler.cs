﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MRSArchis.OpenText;

namespace MRSArchisWFApp.Helpers
{
  public class SessionHandler
  {
    private static string _WFCSAuthClientKey = "WfCSAuthClientKey";
    private static string _WFZaaknummerKey = "WfAppZaaknummerKey";
    private static string _WFUsernameKey = "WfUsernameKey";
    private static string _WFUserDisplayNameKey = "WfUserDisplayNameKey";
    private static string _WFLoginDomainKey = "WfLoginDomainKey";
    private static string _WFUserEmailKey = "WfUserEmailKey";
    private static string _WFWorkItemIdKey = "WfAppWorkItemKey";
    private static string _WFCurrentStepKey = "WfCurrentStepKey";
    private static string _WFProcessReadonlyKey = "WfProcessReadonlyKey";
    private static string _WFProcessInstanceIdKey = "WfProcessInstanceIdKey";
    private static string _WFEPSAccTokenKey = "WfEPSAccTokenKey";
    private static string _WFCSHelperObject = "WFCSHelperObject";

    public static string SessionId
    {
      get
      {
        return HttpContext.Current != null && HttpContext.Current.Session != null ? HttpContext.Current.Session.SessionID : null;
      }
    }

    public static string CaseNumber
    {
      get
      {
        return HttpContext.Current != null && HttpContext.Current.Session != null &&
          HttpContext.Current.Session[_WFZaaknummerKey] != null ? HttpContext.Current.Session[_WFZaaknummerKey].ToString() : "";
      }
      set
      {
        if (HttpContext.Current != null)
          HttpContext.Current.Session[_WFZaaknummerKey] = value;
      }
    }

    public static string WorkItemId
    {
      get
      {
        return HttpContext.Current != null && HttpContext.Current.Session != null &&
          HttpContext.Current.Session[_WFWorkItemIdKey] != null ? HttpContext.Current.Session[_WFWorkItemIdKey].ToString() : "";
      }
      set
      {
        if (HttpContext.Current != null)
          HttpContext.Current.Session[_WFWorkItemIdKey] = value;
      }
    }

    public static string CurrentStep
    {
      get
      {
        return HttpContext.Current != null && HttpContext.Current.Session != null &&
          HttpContext.Current.Session[_WFCurrentStepKey] != null ? HttpContext.Current.Session[_WFCurrentStepKey].ToString() : "";
      }
      set
      {
        if (HttpContext.Current != null)
          HttpContext.Current.Session[_WFCurrentStepKey] = value;
      }
    }

    public static string ProcessInstanceId
    {
      get
      {
        return HttpContext.Current != null && HttpContext.Current.Session != null &&
          HttpContext.Current.Session[_WFProcessInstanceIdKey] != null ?
            HttpContext.Current.Session[_WFProcessInstanceIdKey].ToString() : "";
      }
      set
      {
        if (HttpContext.Current != null)
          HttpContext.Current.Session[_WFProcessInstanceIdKey] = value;
      }
    }

    public static string IsReadonly
    {
      get
      {
        return HttpContext.Current != null && HttpContext.Current.Session != null &&
          HttpContext.Current.Session[_WFProcessReadonlyKey] != null ?
            HttpContext.Current.Session[_WFProcessReadonlyKey].ToString().ToLower() : "false";
      }
      set
      {
        if (HttpContext.Current != null)
          HttpContext.Current.Session[_WFProcessReadonlyKey] = value;
      }
    }

    public static string EPSAccToken
    {
      get
      {
        return HttpContext.Current != null && HttpContext.Current.Session != null &&
          HttpContext.Current.Session[_WFEPSAccTokenKey] != null ?
            HttpContext.Current.Session[_WFEPSAccTokenKey].ToString() : "";
      }
      set
      {
        if (HttpContext.Current != null)
          HttpContext.Current.Session[_WFEPSAccTokenKey] = value;
      }
    }

    public static string CWSAuthToken
    {
      get
      {
        return HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[_WFCSAuthClientKey] != null ?
          HttpContext.Current.Session[_WFCSAuthClientKey].ToString() : "";
      }
      set
      {
        if (HttpContext.Current != null)
          HttpContext.Current.Session[_WFCSAuthClientKey] = value;
      }
    }

    public static string Username
    {
      get
      {
        return HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[_WFUsernameKey] != null ?
          HttpContext.Current.Session[_WFUsernameKey].ToString() : "";
      }
      set
      {
        // Clear token if name has changed
        if (HttpContext.Current != null && HttpContext.Current.Session[_WFUsernameKey] != null &&
            HttpContext.Current.Session[_WFUsernameKey].ToString() != value)
          CWSAuthToken = null;
        HttpContext.Current.Session[_WFUsernameKey] = value;
      }
    }

    public static string UserDisplayName
    {
      get
      {
        return HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[_WFUserDisplayNameKey] != null ?
          HttpContext.Current.Session[_WFUserDisplayNameKey].ToString() : "";
      }
      set
      {
        // Clear token if name has changed
        if (HttpContext.Current != null && HttpContext.Current.Session[_WFUserDisplayNameKey] != null &&
            HttpContext.Current.Session[_WFUserDisplayNameKey].ToString() != value)
          CWSAuthToken = null;
        HttpContext.Current.Session[_WFUserDisplayNameKey] = value;
      }
    }

    public static string UserEmail
    {
      get
      {
        return HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[_WFUserEmailKey] != null ?
          HttpContext.Current.Session[_WFUserEmailKey].ToString() : "";
      }
      set
      {
        // Clear token if name has changed
        if (HttpContext.Current != null && HttpContext.Current.Session[_WFUserEmailKey] != null &&
            HttpContext.Current.Session[_WFUserEmailKey].ToString() != value)
          CWSAuthToken = null;
        HttpContext.Current.Session[_WFUserEmailKey] = value;
      }
    }

    public static string LoginDomain
    {
      get
      {
        return HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[_WFLoginDomainKey] != null ?
          HttpContext.Current.Session[_WFLoginDomainKey].ToString() : "";
      }
      set
      {
        // Clear token if domain has changed
        if (HttpContext.Current != null && HttpContext.Current.Session[_WFLoginDomainKey] != null &&
            HttpContext.Current.Session[_WFLoginDomainKey].ToString() != value)
          CWSAuthToken = null;
        HttpContext.Current.Session[_WFLoginDomainKey] = value;
      }
    }

    public static CSHelper CSHelperObject
    {
      get
      {
        return HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[_WFCSHelperObject] != null ?
          (CSHelper)HttpContext.Current.Session[_WFCSHelperObject] : null;
      }
      set
      {
         HttpContext.Current.Session[_WFCSHelperObject] = value;
      }
    }
  }
}