﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Web;
using System.Web.Mvc;
using MRSArchisOT=MRSArchis.OpenText.Helpers;

namespace MRSArchisWFApp.Helpers
{
  public class UtilityHelper
  {
    private const string numberFields = "";

    private const string dateFields =
      "receiptDate,registrationDate,ontvangstdatum";

    private const int MAX_DEFAULT_UPLOAD_SIZEKB = 10240;


    public static void FormatCollectionForModel(FormCollection collection)
    {
      foreach (string dateField in dateFields.Split(','))
      {
        DateTime? d = MRSArchisOT.UtilityHelper.StringToNullableDate(collection[dateField]);
        if (d.HasValue)
          collection[dateField] = d.ToString();
      }
      foreach (string numberField in numberFields.Split(','))
      {
        Decimal? d = MRSArchisOT.UtilityHelper.StringToNullableDecimal(collection[numberField]);
        if (d.HasValue)
          collection[numberField] = d.ToString();
      }
    }

    public static string AllowedUploadExtensions
    {
      get { return ConfigurationManager.AppSettings["AllowedUploadExtensions"]; }
    }

    public static int UploadMaxSizeKB
    {
      get
      {
        int sizeKB;
        if (int.TryParse(ConfigurationManager.AppSettings["UploadMaxSizeKB"], out sizeKB))
          return sizeKB;
        else
          return MAX_DEFAULT_UPLOAD_SIZEKB;
      }

    }
  }
}