﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Xsl;
using MRSArchis.OpenText;
using MRSArchisWFApp.Models.EPS;
using MRSArchisWFApp.Helpers;
using MRSArchisWFApp.Helpers.EPS;
using MRSArchisWFApp.Helpers.MRSArchis;
using MRSArchisWFApp.MRSArchisWS;

namespace MRSArchisWFApp.Helpers
{
  public class DocumentHelper : CachedDataHelper<document>
  {
    #region log4net
    // Create a logger for this class
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    #endregion

    /// <summary>
    /// Get the cached document list
    /// </summary>
    /// <remarks>Return an empty list if no documents were found</remarks>
    public static List<document> Documents
    {
      get
      {
        if (CacheData == null || !HasSession)
        {
          TrackSession();
          LoadFromDb();
        }
        return CacheData != null ? CacheData.ToList() : new List<document>();
      }
    }
    /// <summary>
    /// Load the document list from the database
    /// </summary>
    private static void LoadFromDb()
    {
      CaseData cdata = CaseDataHelper.CaseData;
      if (cdata.caseTypeId.HasValue)
      {
        ZaakServiceClient zsClient = new ZaakServiceClient();
        var documents = zsClient.findDocument(CaseHelper.GetCaseId(cdata.caseNumber).GetValueOrDefault());

        if (documents != null)
          foreach (var doc in documents)
          {
            long doctypeId = doc.documenttypeId;
            doc.mainType = DocumentTypesHelper.DocumentTypes.FirstOrDefault(dt => dt.id == doctypeId) != null ?
              DocumentTypesHelper.DocumentTypes.FirstOrDefault(dt => dt.id == doctypeId).hoofdDocumenttypeOmschrijving : "Overige";
            doc.subType = DocumentTypesHelper.DocumentTypes.FirstOrDefault(dt => dt.id == doctypeId) != null ?
              DocumentTypesHelper.DocumentTypes.FirstOrDefault(dt => dt.id == doctypeId).subDocumenttypeOmschrijving : "Overige (geen specificatie)";
            AddToCache(new document
              {
                id = doc.id,
                idSpecified = doc.idSpecified,
                mainType = doc.mainType,
                subType = doc.subType,
                documenttypeId = doc.documenttypeId,
                enkelvoudigdocumentId = doc.enkelvoudigdocumentId,
                enkelvoudigdocumentIdSpecified = doc.enkelvoudigdocumentIdSpecified,
                samengestelddocumentId = doc.samengestelddocumentId,
                samengestelddocumentIdSpecified = doc.samengestelddocumentId != 0,
                auteur = doc.auteur,
                beschrijving = doc.beschrijving,
                bestandsgrootte = doc.bestandsgrootte,
                bestandsgrootteSpecified = doc.bestandsgrootteSpecified,
                creatiedatum = doc.creatiedatum,
                creatiedatumSpecified = doc.creatiedatumSpecified,
                bestandsnaam = doc.bestandsnaam,
                formaat = doc.formaat,
                identificatie = doc.identificatie,
                link = doc.link,
                objectNummer = doc.objectNummer,
                ontvangstdatum = doc.ontvangstdatum,
                ontvangstdatumSpecified = doc.ontvangstdatumSpecified,
                registratiedatum = doc.registratiedatum,
                status = doc.status,
                taal = doc.taal,
                titel = doc.titel,
                versie = doc.versie,
                vertrouwelijkheidsAanduidingId = doc.vertrouwelijkheidsAanduidingId,
                verzenddatum = doc.verzenddatum,
                verzenddatumSpecified = doc.verzenddatumSpecified,
                isDirty = false
              });
          }
      }
    }

    public static document AddFile(CuteWebUI.MvcUploadFile file, int nodeId)
    {
      CaseData cdata = CaseDataHelper.CaseData;
      var doc = AddToCache(new document
      {
        auteur = "-",
        titel = Path.GetFileNameWithoutExtension(file.FileName),
        bestandsnaam = Path.GetFileName(file.FileName),
        bestandsgrootte = file.FileSize,
        bestandsgrootteSpecified = true,
        documenttypeId = 1,
        mainType = DocumentTypesHelper.DocumentTypes.FirstOrDefault(dt => dt.id == 1) != null ?
          DocumentTypesHelper.DocumentTypes.FirstOrDefault(dt => dt.id == 1).hoofdDocumenttypeOmschrijving : "Overige",
        subType = DocumentTypesHelper.DocumentTypes.FirstOrDefault(dt => dt.id == 1) != null ?
          DocumentTypesHelper.DocumentTypes.FirstOrDefault(dt => dt.id == 1).subDocumenttypeOmschrijving : "Overige (geen specificatie)",
        link = nodeId.ToString(),
        creatiedatum = DateTime.Now,
        creatiedatumSpecified = true,
        formaat = Path.GetExtension(file.FileName),
        vertrouwelijkheidsAanduidingId = 5,
        registratiedatum = DateTime.Now,
        ontvangstdatum = DateTime.Now,
        ontvangstdatumSpecified = true,
        status = cdata.statusTypeName,
        isNew = true
      });

      // Add to database and update CS
      InsertIntoDB(ref doc, cdata, SessionHandler.Username);
      // Do not use the doc.link here because it will invoke the wrong overload
      SessionHandler.CSHelperObject.UpdateNodeMetadata(new object[] { doc }, nodeId);
      LogHelper.LogDebugInfo("AddFile", doc, log);
      return doc;
    }

    public static document AddFile(HttpPostedFileBase file, int nodeId)
    {
      CaseData cdata = CaseDataHelper.CaseData;
      var doc = AddToCache(new document
      {
        auteur = "-",
        titel = Path.GetFileNameWithoutExtension(file.FileName),
        bestandsnaam = Path.GetFileName(file.FileName),
        bestandsgrootte = file.ContentLength,
        bestandsgrootteSpecified = true,
        documenttypeId = 1,
        mainType = DocumentTypesHelper.DocumentTypes.FirstOrDefault(dt => dt.id == 1).hoofdDocumenttypeOmschrijving,
        subType = DocumentTypesHelper.DocumentTypes.FirstOrDefault(dt => dt.id == 1).subDocumenttypeOmschrijving,
        link = nodeId.ToString(),
        creatiedatum = DateTime.Now,
        creatiedatumSpecified = true,
        formaat = Path.GetExtension(file.FileName),
        vertrouwelijkheidsAanduidingId = 5,
        registratiedatum = DateTime.Now,
        ontvangstdatum = DateTime.Now,
        ontvangstdatumSpecified = true,
        status = cdata.statusTypeName,
        isNew = true,
      });
      // Add to database and update CS
      InsertIntoDB(ref doc, cdata, SessionHandler.Username);
      // Do not use the doc.link here because it will invoke the wrong overload
      SessionHandler.CSHelperObject.UpdateNodeMetadata(new object[] { doc }, nodeId);
      LogHelper.LogDebugInfo("AddFile", doc, log);
      return doc;
    }

    new public static void Delete(string id)
    {
      CaseData cdata = CaseDataHelper.CaseData;
      var doc = GetCacheObject(id);
      if (!string.IsNullOrEmpty(doc.link))
      {
        // Only delete from CS if last attachment
        if (Documents.Where(d => d.link == doc.link).Count() == 1)
          SessionHandler.CSHelperObject.DeleteNode(int.Parse(doc.link));
        DeleteFromDB(doc, cdata.caseNumber, SessionHandler.Username);
        doc.link = string.Empty;
      }
      CachedDataHelper<document>.Delete(id);
    }

    private static void InsertIntoDB(ref document doc, CaseData cdata, string username)
    {
      // Insert the document and update its status fields
      ZaakServiceClient zsClient = new ZaakServiceClient();

      // Add an identifier for this document, but limit its length to max 40 characters
      doc.identificatie = cdata.caseNumber + "_" + doc.bestandsnaam;
      if (doc.identificatie.Length > 40)
        doc.identificatie = doc.identificatie.Substring(0, 40);
      var documentId = zsClient.insertOrUpdateDocument(
        CaseHelper.GetCaseId(cdata.caseNumber).GetValueOrDefault(),
        username,
        cdata.statusTypeId,
        doc
      );
      doc.id = documentId.id;
      doc.idSpecified = documentId.idSpecified;
      doc.enkelvoudigdocumentId = documentId.enkelvoudigdocumentId;
      doc.enkelvoudigdocumentIdSpecified = documentId.enkelvoudigdocumentIdSpecified;
      doc.samengestelddocumentId = documentId.samengestelddocumentId;
      doc.samengestelddocumentIdSpecified = documentId.samengestelddocumentId != 0;
      doc.isNew = false;
      doc.isDirty = false;
    }

    public static void DeleteFromDB(document doc, string caseNumber, string username)
    {
      long docid = doc.id;
      ZaakServiceClient zsClient = new ZaakServiceClient();
      zsClient.deleteDocument(CaseHelper.GetCaseId(caseNumber).GetValueOrDefault(), docid, username);
    }

    /// <summary>
    /// Override the SaveToDB to handle the documents update only
    /// </summary>
    /// <remarks>
    /// Documents in CS have already been removed or inserted by 
    /// the controller logic
    /// </remarks>
    new public static void SaveToDB()
    {
      if (CacheData != null && CacheData.Count() > 0)
      {
        CaseData cdata = CaseDataHelper.CaseData;
        if (!string.IsNullOrEmpty(cdata.caseNumber))
        {
          ZaakServiceClient zsClient = new ZaakServiceClient();

          foreach (var d in CacheData.Where(d => !string.IsNullOrEmpty(d.link) && d.isDirty))
          {
            int nodeId = int.Parse(d.link);
            SessionHandler.CSHelperObject.UpdateNodeMetadata(new object[] { d }, nodeId);
            zsClient.insertOrUpdateDocument(
              CaseHelper.GetCaseId(cdata.caseNumber).GetValueOrDefault(),
              SessionHandler.Username,
              cdata.statusTypeId,
              d
            );
            d.isDirty = false;
          }
        }
        CacheData.RemoveAll(d => d.isDeleted);
        LogHelper.LogDebugInfo("SaveToDB", CacheData, log);
      }
    }

    public static string TransFormXML(int xmlNodeID, string stylesheet)
    {
      var xsltNode = SessionHandler.CSHelperObject.GetFileNodeFromSubFolder(CSHelper.rceMRSRootFolderNickname, "Stylesheets", stylesheet);
      using (XmlReader readerXsl = XmlReader.Create(SessionHandler.CSHelperObject.GetDocument(xsltNode)))
      {
        //readerXsl.ReadToDescendant("xsl:stylesheet");
        XslCompiledTransform xslt = new XslCompiledTransform();
        xslt.Load(readerXsl);
        using (XmlReader readerXml = XmlReader.Create(SessionHandler.CSHelperObject.GetDocumentByNodeId(xmlNodeID)))
        {
          StringBuilder sb = new StringBuilder();
          StringWriter sw = new StringWriter(sb);
          xslt.Transform(readerXml, null, sw);
          return sb.ToString();
        }
      }
    }
  }
}