﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MRSArchis.OpenText.CS.DataAnnotations;

namespace MRSArchisWFApp.Models.CS
{
  public enum DocumentSource
  {
    Eform,
    GMZ,
    ScanningService,
    UserCreated,
    SmartDocuments,
    SF2000,
    Unknown
  }
  [CSCategoryName("Documenttype", "CAT_DIV_DocType")]
  public class AttachmentInfo : Helpers.CachedData
  {
    private int _documentTypeId;
    private string _source = DocumentSource.Unknown.ToString();

    [DisplayName("Monumentnr")]
    public string monumentNumber { get; set; }

    [DisplayName("Titel")]
    public string title { get; set; }

    [DisplayName("Bestandsnaam")]
    public string fileName { get; set; }

    [DisplayName("Aanmaakdatum")]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
    public DateTime? creationDate { get; set; }

    [DisplayName("Subtype")]
    [CSFieldName("Code")]
    public int documentTypeId
    {
      get
      {
        return _documentTypeId;
      }
      set
      {
        if (value != _documentTypeId)
        {
          _documentTypeId = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Hoofdtype")]
    [CSFieldName("Hoofdtype")]
    public string mainType { get; set; }

    [DisplayName("Subtype")]
    [CSFieldName("Subtype")]
    public string subType { get; set; }

    [DisplayName("Download link")]
    public int csObjectId { get; set; }

    [DisplayName("Bron")]
    [CSFieldName("Bron")]
    public string source { get { return _source; } set { _source = value; } }

    [DisplayName("Zaaknummer")]
    public string caseNumber { get; set; }

    #region Public methods
    public override string ToString()
    {
      return Helpers.LogHelper.RenderObject(this);
    }
    #endregion
  }
}