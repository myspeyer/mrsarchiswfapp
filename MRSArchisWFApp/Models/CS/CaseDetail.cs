﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using MRSArchis.OpenText.CS.DataAnnotations;


namespace MRSArchisWFApp.Models.CS
{
  [CSCategoryName("Zaakdetail", "CAT_KIMOMO_Zaakdetail")]
  public class CaseDetail : Helpers.CachedData
  {
    #region private members
    private string _caseTypeName;
    private DateTime? _registrationDate;
    private DateTime? _receiptDate;
    private DateTime? _scanDate;
    private DateTime? _scanReceiptDate;
    private DateTime? _scanRequestCreationDate;
    #endregion

    #region public properties
    [DisplayName("Zaaktype")]
    [CSFieldName("Zaaktype")]
    public string caseType
    {
      get { return _caseTypeName; }
      set
      {
        if (value != _caseTypeName)
        {
          _caseTypeName = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Digitaal")]
    [CSFieldName("Digitaal")]
    public bool digital { get; set; }

    [DisplayName("Registratiedatum")]
    [CSFieldName("Registratiedatum")]
    public DateTime? registrationDate 
    {
      get { return _registrationDate; }
      set
      {
        if (DateTime.Compare(value.GetValueOrDefault(), _registrationDate.GetValueOrDefault()) != 0)
        {
          _registrationDate = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Ontvangstdatum")]
    [CSFieldName("Ontvangstdatum")]
    public DateTime? receiptDate 
    {
      get { return this._receiptDate; }
      set
      {
        if (DateTime.Compare(value.GetValueOrDefault(), this._receiptDate.GetValueOrDefault()) != 0)
        {
          this._receiptDate = value;
          this.isDirty = true;
        }
      }
    }

    [DisplayName("Datum scanopdracht")]
    [CSFieldName("Datum scanopdracht")]
    public DateTime? scanRequestCreationDate
    {
      get { return this._scanRequestCreationDate; }
      set
      {
        if (DateTime.Compare(value.GetValueOrDefault(), this._scanRequestCreationDate.GetValueOrDefault()) != 0)
        {
          this._scanRequestCreationDate = value;
          this.isDirty = true;
        }
      }
    }


    [DisplayName("Scandatum")]
    [CSFieldName("Scandatum")]
    public DateTime? scanDate
    {
      get { return this._scanDate; }
      set
      {
        if (DateTime.Compare(value.GetValueOrDefault(), this._scanDate.GetValueOrDefault()) != 0)
        {
          this._scanDate = value;
          this.isDirty = true;
        }
      }
    }


    [DisplayName("Scan ontvangen op")]
    [CSFieldName("Scan ontvangstdatum")]
    public DateTime? scanReceiptDate
    {
      get { return this._scanReceiptDate; }
      set
      {
        if (DateTime.Compare(value.GetValueOrDefault(), this._scanReceiptDate.GetValueOrDefault()) != 0)
        {
          this._scanReceiptDate = value;
          this.isDirty = true;
        }
      }
    }

    [DisplayName("Datum afgerond")]
    [CSFieldName("Datum afgerond")]
    public DateTime? dateCompleted { get; set; }

    #endregion

    #region Public methods
    public override string ToString()
    {
      return Helpers.LogHelper.RenderObject(this);
    }
    #endregion
  }

  enum CaseLocations
  {
    DIV,
    Subsidies
  };
}