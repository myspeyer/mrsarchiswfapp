﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRSArchisWFApp.Models.CaseService
{
  public class CaseDetail
  {
    [DisplayName("zaak_id")]
    public int caseId { get; set; }

    [DisplayName("status")]
    public int status { get; set; }

    [DisplayName("zaak_type")]
    public int caseTypeId { get; set; }

    [DisplayName("subregeling_aanvraagjaar_id")]
    public int? caseYearNameId { get; set; }

    [DisplayName("subsidie_id")]
    public int? subsidyId { get; set; }

    public int[] monumentIds;
  }
}