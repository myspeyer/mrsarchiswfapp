﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRSArchisWFApp.Models.CaseService
{
  public enum CaseStates
  {
    Submitted = 2,
    InProgress = 3,
    Rejected = 4,
    Approved = 5
  }
}