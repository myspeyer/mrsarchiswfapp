﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MRSArchisWFApp.Models.CaseService
{
  public enum CaseTypes
  {
    RCE_START_ZAAK = 1, 
    RCE_MELDING = 2, 
    RCE_WIJZIGING = 3, 
  };
}