﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRSArchisWFApp.Models.CaseService
{
  public enum ChangeTypes
  {
    CORRECTIE,
    AANVULLING,
    WIJZIGINGSVERZOEK,
  };
}