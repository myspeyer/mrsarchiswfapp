﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MRSArchisWFApp.MRSArchisWS
{
  #region Metadata definition
  public class documentMetadata
  {
    [DisplayName("Auteur")]
    public string auteur { get; set; }

    [DisplayName("Beschrijving")]
    public string beschrijving { get; set; }

    [DisplayName("Bestandsnaam")]
    public string bestandsnaam { get; set; }

    [DisplayName("Bestandsgrootte")]
    public long bestandsgrootte { get; set; }

    [DisplayName("Creatiedatum")]
    [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
    public DateTime creatiedatum { get; set; }

    [DisplayName("Ontvangstdatum")]
    [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
    public DateTime ontvangstdatum { get; set;}

    [DisplayName("Subtype")]
    public long documenttypeId { get; set; }

    [DisplayName("Link in DMS")]
    public string link { get; set; }
  }
  #endregion

  #region Metadata application
  [MetadataType(typeof(documentMetadata))]
  public partial class document
  {
  }
  #endregion
}