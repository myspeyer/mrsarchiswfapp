﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using MRSArchis.OpenText.CS.DataAnnotations;

namespace MRSArchisWFApp.MRSArchisWS
{
  [CSCategoryName("Documenttype", "CAT_MRS_DocType")]
  public partial class document : Helpers.ICachedData
  {
    #region private members
    private string _mainType;
    private string _subType;
    #endregion

    #region interface implementation
    [XmlIgnore()]
    public string cacheId { get; set; }

    [XmlIgnore()]
    public string sessionId { get; set; }

    [XmlIgnore()]
    public string workItemId { get; set; }

    [XmlIgnore()]
    public string currentStep { get; set; }

    [XmlIgnore()]
    public bool isDirty { get; set; }

    [XmlIgnore()]
    public bool isNew { get; set; }

    [XmlIgnore()]
    public bool isDeleted { get; set; }

    [XmlIgnore()]
    public bool isReadOnly { get; set; }
    #endregion

    #region Extra properties
    [XmlIgnore()]
    [DisplayName("Hoofdtype")]
    [CSFieldName("Hoofdtype")]
    public string mainType
    {
      get { return _mainType; }
      set
      {
        if (value != _mainType)
        {
          _mainType = value;
          isDirty = true;
        }
      }
    }

    [XmlIgnore()]
    [DisplayName("Subtype")]
    [CSFieldName("Subtype")]
    public string subType
    {
      get { return _subType; }
      set
      {
        if (value != _subType)
        {
          _subType = value;
          isDirty = true;
        }
      }
    }
    #endregion
  }
}