﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRSArchisWFApp.Models.Checklist
{
    public enum Answer
    {
        No = 0,
        Yes = 1,
        NotApplicable = 2,
        Checked = 3,
        Unchecked = 4,
        NotYetDecided = 5
    }
}