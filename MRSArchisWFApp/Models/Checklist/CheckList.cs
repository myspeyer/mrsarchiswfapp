﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace MRSArchisWFApp.Models.Checklist
{
  public class CheckList : Helpers.CachedData
  {
    public string WorkflowStepName { get; set; }
    public string Name { get; set; }
    public List<Question> Questions { get; set; }
  }
}









