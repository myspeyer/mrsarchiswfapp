﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRSArchisWFApp.Models.Checklist
{
    public class Question
    {
        public string Code { get; set; }
        public string Description { get; set;}
        public QuestionType Type { get; set;}
        public Answer Answer { get; set; }
        public List<Question> SubQuestions { get; set; }
    }
}