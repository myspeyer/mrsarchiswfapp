﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRSArchisWFApp.Models.Checklist
{
    public enum QuestionType
    {
        YesNo = 0,
        YesNoNotApplicable = 1,
        Checkbox = 2
    }
}