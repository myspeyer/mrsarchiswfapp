﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MRSArchisWFApp.CHOService
{
    #region Metadata definition
    public class geometryMetadata
    {
        [DisplayName("Herkomst")]
        public int herkomstId { get; set; }

        [DisplayName("Kwaliteit geometrie")]
        public int kwaliteitGeometrieId { get; set; }

        [DisplayName("Mate overname")]
        public int mateOvernameId { get; set; }

        [DisplayName("Status geometrie")]
        public int statusGeometrieId { get; set; }
    }
    #endregion

    #region Metadata application
    [MetadataType(typeof(geometryMetadata))]
    public partial class geometrie
    {
    }
    #endregion
}