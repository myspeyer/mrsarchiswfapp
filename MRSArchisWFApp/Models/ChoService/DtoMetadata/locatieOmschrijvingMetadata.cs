﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MRSArchisWFApp.CHOService
{
    #region Metadata definition
    public class locatieOmschrijvingMetadata
    {
        [DisplayName("Omschrijving")]
        public string omschrijving { get; set; }
    }
    #endregion

    #region Metadata application
    [MetadataType(typeof(locatieOmschrijvingMetadata))]
    public partial class locatieOmschrijving
    {
    }
    #endregion
}