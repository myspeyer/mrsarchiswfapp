﻿using MRSArchisWFApp.Helpers.CHO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace MRSArchisWFApp.CHOService
{
    public partial class geometrie : Helpers.ICachedData
    {
        public string statusGeometrie
        {
            get
            {
                return statusGeometrieIdSpecified ? StatesGeometryHelper.getStateGeometryWaarde(statusGeometrieId) : string.Empty;
            }
        }

        #region interface implementation
        [XmlIgnore()]
        public string cacheId { get; set; }

        [XmlIgnore()]
        public string sessionId { get; set; }

        [XmlIgnore()]
        public string workItemId { get; set; }

        [XmlIgnore()]
        public string currentStep { get; set; }

        [XmlIgnore()]
        public bool isDirty { get; set; }

        [XmlIgnore()]
        public bool isNew { get; set; }

        [XmlIgnore()]
        public bool isDeleted { get; set; }

        [XmlIgnore()]
        public bool isReadOnly { get; set; }
        #endregion
    }
}