﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace MRSArchisWFApp.CHOService
{
    public partial class rijksmonument : Helpers.ICachedData
    {
        [XmlIgnore()]
        public geometrie geometrie; 
        
        #region interface implementation
        [XmlIgnore()]
        public string cacheId { get; set; }

        [XmlIgnore()]
        public string sessionId { get; set; }

        [XmlIgnore()]
        public string workItemId { get; set; }

        [XmlIgnore()]
        public string currentStep { get; set; }

        [XmlIgnore()]
        public bool isDirty { get; set; }

        [XmlIgnore()]
        public bool isNew { get; set; }

        [XmlIgnore()]
        public bool isDeleted { get; set; }

        [XmlIgnore()]
        public bool isReadOnly { get; set; }
        #endregion
    }
}