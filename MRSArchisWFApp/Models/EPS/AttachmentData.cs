﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MRSArchis.OpenText.EPS.DataAnnotations;


namespace MRSArchisWFApp.Models.EPS
{
  [EPSDataObject("Bijlagen")]
  public class AttachmentData : Helpers.CachedData
  {
    [EPSFieldName("Monumentnummers")]
    public string[] MonumentNumbers { get; set; }

    [EPSFieldName("Titels")]
    public string[] Titles { get; set; }

    [EPSFieldName("CSObjectIds")]
    public int?[] CSObjectIds { get; set; }

    [EPSFieldName("Documenttypes")]
    public string[] DocumentTypes { get; set; }

    [EPSFieldName("DocumenttypeIds")]
    public int?[] DocumentTypeIds { get; set; }

    [EPSFieldName("Aanmaakdatum")]
    public DateTime?[] CreationTime { get; set; }

    [EPSFieldName("Bron")]
    public string[] Source { get; set; }

    #region Public methods
    public override string ToString()
    {
      return Helpers.LogHelper.RenderObject(this);
    }
    #endregion
  }
}