﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Text;
using MRSArchis.OpenText.EPS.DataAnnotations;
using MRSArchis.OpenText.CS.DataAnnotations;

namespace MRSArchisWFApp.Models.EPS
{
  [EPSDataObject("Zaak")]
  [CSCategoryName("Hoofdzaak", "CAT_KIMOMO_Hoofdzaak")]
  public class CaseData : Helpers.CachedData
  {
    #region private members
    private string _targetCaseTypeName;
    private DateTime _replyDate = DateTime.Now;
    private int _decisionPeriod;
    private int _numAttachments;
    private DateTime? _senderDate;
    private string _senderReference;
    private bool _complete;
    private DateTime? _dateSigned = DateTime.Now;
    private DateTime? _dateDecision = DateTime.Now;
    private string _emailCaseworker;
    private string _emailSignatory;
    private bool _restart;
    private bool? _receiptNote;
    private DateTime? _receiptDate = DateTime.Now;
    private bool _invalidMail;
    private string _region;
    private DateTime? _registrationDate = DateTime.Now;
    private string _roleTypeName;
    private bool? _scanRequest;
    private string _scanNumber;
    private DateTime? _scanReceiptDate;
    private string _statusTypeName;
    private bool _expired;
    private bool _postpone;
    private DateTime? _postponeDate = null;
    private string _previousCaseNumber;
    private DateTime? _expiryDate;
    private bool _waiting;
    private string _caseNumber;
    private string _caseTypeName;
    #endregion

    #region Helper fields (non=EPS Data)
    public string activeCheckList { get; set; }
    #endregion

    #region EPS Process data and CS attributes fields

    [DisplayName("Beslistermijn (weken)")]
    [EPSFieldName("Beslistermijn")]
    public int decisionPeriod
    {
      get { return _decisionPeriod; }
      set
      {
        if (value != _decisionPeriod)
        {
          _decisionPeriod = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Aantal bijlagen")]
    [EPSFieldName("Bijlagen")]
    public int numAttachments
    {
      get { return _numAttachments; }
      set
      {
        if (value != _numAttachments)
        {
          _numAttachments = value;
          isDirty = true;
        }
      }
    }

    [EPSFieldName("Briefcategorie")]
    public string letterCategory { get; set; }

    [DisplayName("Briefdatum (uw datum)")]
    [EPSFieldName("Briefdatum")]
    public DateTime? senderDate
    {
      get { return this._senderDate; }
      set
      {
        if (DateTime.Compare(value.GetValueOrDefault(), _senderDate.GetValueOrDefault()) != 0)
        {
          _senderDate = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Briefkenmerk (uw kenmerk)")]
    [EPSFieldName("Briefkenmerk")]
    public string senderReference
    {
      get { return _senderReference; }
      set
      {
        if (value != _senderReference)
        {
          _senderReference = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Compleet")]
    [EPSFieldName("Compleet")]
    public bool complete
    {
      get { return _complete; }
      set
      {
        if (value != _complete)
        {
          _complete = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Dagtekening")]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
    [Required(ErrorMessage = "Dagtekening is verplicht", AllowEmptyStrings = false)]
    [EPSFieldName("Dagtekening")]
    public DateTime? dateSigned
    {
      get { return _dateSigned; }
      set
      {
        if (DateTime.Compare(value.GetValueOrDefault(), _dateSigned.GetValueOrDefault()) != 0)
        {
          _dateSigned = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Digitaal ingediend?")]
    [EPSFieldName("Digitaal")]
    public bool isDigital { get; set; }

    [DisplayName("Documentnaam")]
    [EPSFieldName("Documentnaam")]
    public string documentName { get; set; }

    [DisplayName("Documenttype")]
    [EPSFieldName("Documenttype")]
    public string documenttype { get; set; }

    [DisplayName("DocumenttypeId")]
    [EPSFieldName("DocumenttypeId")]
    public int documenttypeId { get; set; }

    [DisplayName("Download link")]
    public int? CSObjectId { get; set; }

    [DisplayName("Email behandelaar")]
    [EPSFieldName("EmailBehandelaar")]
    public string emailCaseworker
    {
      get { return _emailCaseworker; }
      set
      {
        if (value != _emailCaseworker)
        {
          _emailCaseworker = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Email ondertekenaar")]
    [EPSFieldName("EmailOndertekenaar")]
    public string emailSignatory
    {
      get { return _emailSignatory; }
      set
      {
        if (value != _emailSignatory)
        {
          _emailSignatory = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Gerelateerd zaaknummer")]
    public string caseReference { get; set; }

    [DisplayName("Herstart")]
    [EPSFieldName("Herstart")]
    public bool restart
    {
      get { return _restart; }
      set
      {
        if (value != _restart)
        {
          _restart = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("In behandeling")]
    public bool inProgress { get; set; }

    [DisplayName("Bericht/Notificatie")]
    [EPSFieldName("Notificatie")]
    [CSFieldName("Notificatie")]
    public string notification { get; set; }

    [DisplayName("Ontvangstbevestging versturen")]
    [EPSFieldName("Ontvangstbevestiging")]
    public bool? receiptNote
    {
      get { return _receiptNote; }
      set
      {
        if (value != _receiptNote)
        {
          _receiptNote = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Datum binnengekomen")]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
    [Required(ErrorMessage = "Ontvangstdatum is verplicht", AllowEmptyStrings = false)]
    [EPSFieldName("Ontvangstdatum")]
    public DateTime? receiptDate
    {
      get { return this._receiptDate; }
      set
      {
        if (DateTime.Compare(value.GetValueOrDefault(), this._receiptDate.GetValueOrDefault()) != 0)
        {
          this._receiptDate = value;
          this.isDirty = true;
        }
      }
    }

    [DisplayName("Ontvangstdatum scan")]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
    public DateTime? scanReceiptDate
    {
      get { return _scanReceiptDate; }
      set
      {
        if (DateTime.Compare(value.GetValueOrDefault(), _scanReceiptDate.GetValueOrDefault()) != 0)
        {
          _scanReceiptDate = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Onjuist poststuk")]
    [EPSFieldName("PoststukOnjuist")]
    public bool invalidMail
    {
      get { return _invalidMail; }
      set
      {
        if (value != _invalidMail)
        {
          _invalidMail = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Regio")]
    [EPSFieldName("Regio")]
    [CSFieldName("Regio")]
    public string region
    {
      get { return _region; }
      set
      {
        if (value != _region)
        {
          _region = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Registratiedatum")]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
    [Required(ErrorMessage = "Registratiedatum is verplicht", AllowEmptyStrings = false)]
    [EPSFieldName("Registratiedatum")]
    public DateTime? registrationDate
    {
      get { return _registrationDate; }
      set
      {
        if (DateTime.Compare(value.GetValueOrDefault(), _registrationDate.GetValueOrDefault()) != 0)
        {
          _registrationDate = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Roltype")]
    [EPSFieldName("Roltype")]
    public string roleTypeName
    {
      get { return _roleTypeName; }
      set
      {
        if (value != _roleTypeName)
        {
          _roleTypeName = value;
          isDirty = true;
        }
      }
    }

    public long roleTypeId { get; set; }

    [DisplayName("Scanopdracht maken")]
    [EPSFieldName("Scanopdracht")]
    public bool? scanRequest
    {
      get { return _scanRequest; }
      set
      {
        if (value != _scanRequest)
        {
          _scanRequest = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Scannummer(verkort, 10 cijfers)")]
    [EPSFieldName("Scannummer")]
    public string scanNumber
    {
      get { return _scanNumber; }
      set
      {
        if (value != _scanNumber)
        {
          _scanNumber = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Sjabloon")]
    [EPSFieldName("Sjabloon")]
    public string letterTemplate { get; set; }

    [DisplayName("Huidige status")]
    [EPSFieldName("Statustype")]
    public string statusTypeName
    {
      get { return _statusTypeName; }
      set
      {
        if (value != _statusTypeName)
        {
          _statusTypeName = value;
          isDirty = true;
        }
      }
    }

    public long statusTypeId { get; set; }

    [DisplayName("Termijn verstreken")]
    [EPSFieldName("TermijnVerstreken")]
    public bool expired
    {
      get { return _expired; }
      set
      {
        if (value != _expired)
        {
          _expired = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Uitstel")]
    [EPSFieldName("Uitstel")]
    public bool postpone
    {
      get { return _postpone; }
      set
      {
        if (value != _postpone)
        {
          _postpone = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Uitsteldatum")]
    [EPSFieldName("Uitsteldatum")]
    public DateTime? postponeDate
    {
      get { return _postponeDate; }
      set
      {
        if (DateTime.Compare(value.GetValueOrDefault(), _postponeDate.GetValueOrDefault()) != 0)
        {
          _postponeDate = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Vervaldatum")]
    [EPSFieldName("Vervaldatum")]
    public DateTime? expiryDate
    {
      get { return _expiryDate; }
      set
      {
        if (DateTime.Compare(value.GetValueOrDefault(), _expiryDate.GetValueOrDefault()) != 0)
        {
          _expiryDate = value;
          isDirty = true;
        }
      }
    }

    [EPSFieldName("Vervolgstatustype")]
    [DisplayName("Status voor vervolgbehandeling")]
    [Required(ErrorMessage = "Kies een status voor verdere behandeling")]
    public string targetCaseTypeName
    {
      get { return _targetCaseTypeName; }
      set
      {
        if (value != _targetCaseTypeName)
        {
          _targetCaseTypeName = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Verzenddatum")]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
    public DateTime? dateSend { get; set; }

    [DisplayName("Vorig zaaknummer")]
    [EPSFieldName("VorigZaaknummer")]
    public string previousCaseNumber
    {
      get { return _previousCaseNumber; }
      set
      {
        if (value != _previousCaseNumber)
        {
          _previousCaseNumber = value;
          isDirty = true;
        }
      }
    }

    /// <summary>
    /// Flag to indicate the case is wating fot a response. Used for display
    /// and workflow routing purposes.
    /// </summary>
    /// <remarks>
    /// This flag is also set in a workflow web service call and then used
    /// to update the process data.
    /// </remarks>
    [DisplayName("Wachtstand")]
    [EPSFieldName("Wachtstand")]
    public bool waiting 
    {
      get { return _waiting; }
      set
      {
        if (value != _waiting)
        {
          _waiting = value;
          isDirty = true;
        }
      }
    }

    [EPSFieldName("WorkflowDoc")]
    [DisplayName("Workflow bijlage")]
    public string workflowDoc { get; set; }

    [DisplayName("Zaaknummer (verkort, 10 cijfers)")]
    [StringLength(10, ErrorMessage = "Zaaknummer moet uit 10 posities bestaan", MinimumLength = 10)]
    [Required(ErrorMessage = "Zaaknummer is verplicht", AllowEmptyStrings = false)]
    [EPSFieldName("Zaaknummer")]
    public string caseNumber
    {
      get { return _caseNumber; }
      set
      {
        if (value != _caseNumber)
        {
          _caseNumber = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Zaaktype")]
    [EPSFieldName("Zaaktype")]
    public string caseTypeName
    {
      get { return _caseTypeName; }
      set
      {
        if (value != _caseTypeName)
        {
          _caseTypeName = value;
          isDirty = true;
        }
      }
    }

    /// <summary>
    /// Holds the id of the active case
    /// </summary>
    public long? caseTypeId { get; set; }

    #endregion

    #region Public methods
    public override string ToString()
    {
      return Helpers.LogHelper.RenderObject(this);
    }
    #endregion
  }
}