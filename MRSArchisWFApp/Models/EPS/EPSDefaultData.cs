﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MRSArchis.OpenText.EPS.DataAnnotations;

namespace MRSArchisWFApp.Models.EPS
{
  [EPSDataObject("_default")]
  public class EPSDefaultData
  {
    [EPSFieldName("CaseUserName")]
    public string caseUserName { get; set; }

    [EPSFieldName("RetryCount")]
    public int? retryCount { get; set; }

    [EPSFieldName("Steekproefresultaat")]
    public int? sampleResult { get; set; }

    #region Public methods
    public override string ToString()
    {
      return Helpers.LogHelper.RenderObject(this);
    }
    #endregion
  }
}