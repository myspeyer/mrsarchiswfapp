﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using MRSArchisWFApp.Helpers;

namespace MRSArchisWFApp.Models
{
  public class Finding : CachedData
  {
    private string _description;
    private bool _restart;

    [DisplayName("Datum")]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
    public DateTime Date { get; set; }

    [DisplayName("Bevinding")]
    public string Description
    {
      get { return _description; }
      set
      {
        if (value != _description)
        {
          _description = value;
          isDirty = true;
        }
      }
    }

    [DisplayName("Opnieuw behandelen")]
    public bool Restart
    {
      get { return _restart; }
      set
      {
        if (value != _restart)
        {
          _restart = value;
          isDirty = true;
        }
      }
    }


    [DisplayName("Actieve checklijst")]
    public string Checklist { get; set; }

    [DisplayName("Aangemaakt door")]
    public string Creator { get; set; }

    public string WorkflowStepName { get; set; }

    #region Public methods
    public override string ToString()
    {
      return LogHelper.RenderObject(this);
    }
    #endregion
  }
}