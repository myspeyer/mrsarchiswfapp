﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using MRSArchisWFApp.Models.CS;

namespace MRSArchisWFApp.Models
{
  public class ImportResult
  {
    public string CaseNumber { get; set; }
    public Models.CaseService.CaseTypes? CaseType { get; set; }
    public string CaseName { get; set; }
    public string SubCaseName { get; set; }
    public int CaseYearNameId { get; set; }
    public string CaseYearName { get; set; }
    public string SenderReference { get; set; }
    public bool IsDigital { get; set; }
    public DateTime SenderDate { get; set; }
    public DateTime ReceiptDate { get; set; }
    public DateTime RegistrationDate { get; set; }
    public string ScanningService { get; set; }
    public string ProcessClassName { get; set; }
    public List<AttachmentInfo> Attachments { get; set;}
    public List<string> Log { get; set; }

    public override string ToString()
    {
      StringBuilder sb = new StringBuilder();

      PropertyInfo[] props = this.GetType().GetProperties();
      foreach (PropertyInfo prop in props)
      {
        if (sb.Length != 0) sb.Append(", ");
        switch (prop.PropertyType.FullName)
        {
          case "System.String":
            sb.AppendFormat("{0}: {1} ", prop.Name, (string)prop.GetValue(this, null));
            break;

          case "System.Int16":
          case "System.Int32":
            sb.AppendFormat("{0}: {1} ", prop.Name, ((int)prop.GetValue(this, null)));
            break;

          case "System.Int64":
            sb.AppendFormat("{0}: {1} ", prop.Name, ((long)prop.GetValue(this, null)));
            break;

          case "System.Boolean":
            sb.AppendFormat("{0}: {1} ", prop.Name, ((bool)prop.GetValue(this, null)));
            break;

          case "System.DateTime":
            sb.AppendFormat("{0}: {1} ", prop.Name, ((DateTime)prop.GetValue(this, null)).ToUniversalTime());
            break;

          case "System.String[]":
            sb.AppendLine();
            sb.AppendLine(prop.Name + ":");
            foreach (string s in (string[])prop.GetValue(this, null))
              sb.AppendLine(s);
            break;
        }
      }
      sb.AppendLine("Bijlagen:");
      if (Attachments != null)
        foreach (AttachmentInfo d in Attachments)
          sb.AppendFormat("Titel {0}, AanmaakDatum: {1}, DocumentType: {2}, CS objectid: {3} \n",
            d.title, d.creationDate.GetValueOrDefault().ToString("s"), d.documentTypeId, d.csObjectId);
      return sb.ToString();
    }
  }

}