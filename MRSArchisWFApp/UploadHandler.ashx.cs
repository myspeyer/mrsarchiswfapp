﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using CuteWebUI;

namespace MRSArchisWFApp
{
  /// <summary>
  /// Summary description for UploadHandler
  /// </summary>
  public class UploadHandler : CuteWebUI.MvcHandler
  {
    public override UploaderValidateOption GetValidateOption()
    {
      CuteWebUI.UploaderValidateOption option = new UploaderValidateOption();
      option.MaxSizeKB = 100 * 1024;
      option.AllowedFileExtensions = Helpers.UtilityHelper.AllowedUploadExtensions;
      return option;
    }

    public override void OnFileUploaded(MvcUploadFile file)
    {
      if (string.Equals(Path.GetExtension(file.FileName), ".exe", StringComparison.OrdinalIgnoreCase) ||
        string.Equals(Path.GetExtension(file.FileName), ".com", StringComparison.OrdinalIgnoreCase) ||
        string.Equals(Path.GetExtension(file.FileName), ".js", StringComparison.OrdinalIgnoreCase))
      {
        file.Delete();
        throw (new Exception("Het uploaden van EXE, COM en Javascript bestanden is niet toegestaan"));
      }

      this.SetServerData("this value will pass to javascript api as item.ServerData");

      //  TODO:use methods
      //  to move the file to somewhere
      //file.MoveTo("~/newfolder/" + file.FileName);

      //  or move to somewhere
      //file.CopyTo("~/newfolder/" + file.FileName);

      //  or delete it
      //file.Delete()

      //get the file properties
      //file.FileGuid
      //file.FileSize
      //file.FileName

      //use this method to open an file stream
      //file.OpenStream

    }

    public override void OnUploaderInit(MvcUploader uploader)
    {

    }
  }
}